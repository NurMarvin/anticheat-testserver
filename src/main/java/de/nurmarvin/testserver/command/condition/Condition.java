package de.nurmarvin.testserver.command.condition;

import de.nurmarvin.testserver.entities.Player;

public interface Condition {
    boolean canProcess(Player player, String[] args);
}
