package de.nurmarvin.testserver.command.condition;

import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;

import java.beans.ConstructorProperties;

public class RankCondition
        implements Condition {
    private final Rank rank;

    @ConstructorProperties({"rank"})
    public RankCondition(Rank rank) {
        this.rank = rank;
    }

    public boolean canProcess(Player player, String[] args) {
        if (!player.hasRank(this.rank)) {
            return false;
        }
        return true;
    }
}
