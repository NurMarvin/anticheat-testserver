package de.nurmarvin.testserver.command;

import de.nurmarvin.testserver.entities.Player;

public interface CommandExecutable {
    void execute(Player player, String[] args);
}
