package de.nurmarvin.testserver.command;

import com.google.common.collect.Lists;
import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.command.condition.Condition;
import de.nurmarvin.testserver.entities.Player;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Command
        implements CommandExecutable {
    private List<String> knownNames = Lists.newArrayList();
    private List<Condition> conditions = Lists.newArrayList();
    private List<Command> subCommands = Lists.newArrayList();

    public Command(String name) {
        addAlias(name);
    }

    public void addAlias(String command) {
        String lowercase = command.toLowerCase();
        if (this.knownNames.contains(lowercase)) {
            throw new IllegalArgumentException("Alias can not duplicate");
        }
        this.knownNames.add(lowercase);
    }

    public List<String> getAliases() {
        return this.knownNames;
    }

    public String getName() {
        return (String) this.knownNames.get(0);
    }

    public void addCondition(Condition condition, String... args) {
        if (args.length == 0) {
            this.conditions.add(condition);
            return;
        }
        String lowercase = args[0].toLowerCase();

        Command sub = null;
        for (Command commands : this.subCommands) {
            if (commands.knownNames.contains(lowercase)) {
                sub = commands;
            }
        }
        if (sub != null) {
            String[] newArgs = Arrays.copyOfRange(args, 1, args.length);
            sub.addCondition(condition, newArgs);
            return;
        }
        this.conditions.add(condition);
    }

    public void addSubCommand(Command command) {
        this.subCommands.add(command);
    }

    public void execute(Player player, String[] args) {
        for (Condition condition : this.conditions) {
            if (!condition.canProcess(player, args)) {
                return;
            }
        }
        if (args.length == 0) {
            if (args.length >= minArgs()) {
                process(player, args);
            } else {
                player.sendMessage(describeUsage());
            }
            return;
        }
        String lowercase = args[0].toLowerCase();

        Command sub = null;
        for (Command commands : this.subCommands) {
            if (commands.knownNames.contains(lowercase)) {
                sub = commands;
            }
        }
        if (sub != null) {
            String[] newArgs = Arrays.copyOfRange(args, 1, args.length);
            sub.execute(player, newArgs);
            return;
        }
        if (args.length >= minArgs()) {
            process(player, args);
        } else {
            player.sendMessage(describeUsage());
        }
    }

    public void process(Player player, String[] args) {
        process(player.getBukkitPlayer(), args);
    }

    public void process(CommandSender commandSender, String[] args) {
        if (args.length == 0) {
            commandSender.sendMessage(describeUsage());
            if (describeSubCommands() == null) return;
            commandSender.sendMessage(ChatColor.RED + describeSubCommands());
        } else {
            commandSender.sendMessage(ChatColor.RED + "Unknown Subcommand");
            if (describeSubCommands() == null) return;
            commandSender.sendMessage(ChatColor.RED + describeSubCommands());
        }
    }

    private String describeSubCommands() {
        if (this.subCommands.isEmpty()) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        builder.append("/");
        builder.append(this.knownNames.get(0));
        builder.append(" <");
        for (Command commands : this.subCommands) {
            builder.append(commands.knownNames.get(0));
            builder.append(", ");
        }
        builder.delete(builder.length() - 2, builder.length());
        builder.append(">");
        return builder.toString();
    }

    public int minArgs() {
        return 0;
    }

    public List<String> suggest(Player player, String[] args) {
        if (this.subCommands.isEmpty()) {
            return Arrays.asList();
        }

        String lowercase = args[0].toLowerCase();

        Command sub = null;
        for (Command commands : this.subCommands) {
            if (commands.knownNames.contains(lowercase)) {
                sub = commands;
            }
        }
        if (sub != null) {
            String[] newArgs = Arrays.copyOfRange(args, 1, args.length);
            return sub.suggest(player, newArgs);
        }

        List<String> list = new ArrayList();
        for (Command commands : this.subCommands) {
            list.add(commands.knownNames.get(0));
        }

        return list;
    }

    protected boolean playerProvided(Player sender, int arg, String[] args) {
        if (args.length < arg) {
            sender.sendMessage(describeUsage());
            return false;
        }

        return Testserver.getInstance().getPlayerManager().getPlayer(args[arg]) != null;
    }

    protected List<String> getPlayerSuggestions(String arg) {
        return StringUtil.copyPartialMatches(arg, Arrays.asList(Arrays.stream(Testserver.getInstance().
                                                     getRealServer().getPlayers()).map(Player::getName).toArray(String[]::new)),
                                             new ArrayList<>(
                                                     Testserver.getInstance().getRealServer().getPlayers().length));
    }

    public String describeUsage() {
        return ChatColor.RED + "Too few arguments!";
    }
}
