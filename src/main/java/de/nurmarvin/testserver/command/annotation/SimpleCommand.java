package de.nurmarvin.testserver.command.annotation;

import de.nurmarvin.testserver.permissions.Rank;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface SimpleCommand {
    Rank requiredRank() default Rank.USER;

    String name();

    String[] aliases() default {};
}
