package de.nurmarvin.testserver.command.annotation;

import de.nurmarvin.testserver.command.CommandExecutable;
import de.nurmarvin.testserver.command.condition.Condition;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class SimpleCommandWrapper
        implements CommandExecutable {
    private Object object;
    private Method method;
    private Rank rank;
    private List<Condition> conditions;

    public SimpleCommandWrapper(Object object, Method method, Rank rank) {
        this.conditions = new ArrayList();
        this.object = object;
        (this.method = method).setAccessible(true);
        this.rank = rank;
    }

    public void addCondition(Condition condition) {
        this.conditions.add(condition);
    }

    public void execute(Player player, String[] args) {
        try {
            if (!player.hasRank(rank)) {
                return;
            }
            for (Condition condition : this.conditions) {
                if (!condition.canProcess(player, args)) {
                    return;
                }
            }
            this.method.invoke(this.object, new Object[]{player, args});
        } catch (Throwable ex) {}
    }
}
