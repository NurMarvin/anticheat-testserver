package de.nurmarvin.testserver.commands.mod;

import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.command.condition.RankCondition;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;

public class BuildCommand extends Command {
    public BuildCommand() {
        super("build");
        addCondition(new RankCondition(Rank.MOD));
    }

    @Override
    public void process(Player player, String[] args) {
        player.setCanBuild(!player.getCanBuild());
        player.sendMessage("§eYou can " + (player.getCanBuild() ? "now" : "no longer") + " build!");
    }
}
