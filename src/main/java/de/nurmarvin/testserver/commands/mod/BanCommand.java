package de.nurmarvin.testserver.commands.mod;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.command.condition.RankCondition;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;

public class BanCommand extends Command {
    public BanCommand() {
        super("ban");
        this.addCondition(new RankCondition(Rank.MOD));
    }

    @Override
    public void process(Player player, String[] args) {
        switch (args.length) {
            case 0: {
                player.sendMessage("§c/ban <Player> [Reason...]");
                break;
            }
            case 1: {
                Player target = Testserver.getInstance().getPlayerManager().getPlayer(args[0]);

                if (target == null) {
                    player.sendMessage("§cCan't find that player.");
                    return;
                }
                target.setBanned(true);
                target.setBanReason("Undefined");

                target.upload();

                player.sendMessage(
                        target.getNameWithPrefix() + "§e was banned with the reason " + target.getBanReason());

                break;
            }
            default: {
                Player target = Testserver.getInstance().getPlayerManager().getPlayer(args[0]);
                StringBuilder reason = new StringBuilder();

                if (target == null) {
                    player.sendMessage("§cCan't find that player.");
                    return;
                }
                Testserver.debug("Banning player...");
                target.setBanned(true);
                Testserver.debug("Banned: " + target.isBanned());

                for (int i = 1; i < args.length; i++) {
                    if (i != 1)
                        reason.append(" ");
                    reason.append(args[i]);
                }
                target.setBanReason(reason.toString());

                target.upload();

                player.sendMessage(
                        target.getNameWithPrefix() + "§e was banned with the reason " + target.getBanReason());
                break;
            }
        }
    }
}
