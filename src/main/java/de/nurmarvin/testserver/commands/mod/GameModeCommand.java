package de.nurmarvin.testserver.commands.mod;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.command.condition.RankCondition;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;
import org.bukkit.GameMode;

public class GameModeCommand extends Command {
    public GameModeCommand() {
        super("gamemode");
        addAlias("gm");
        addCondition(new RankCondition(Rank.MOD));
    }

    @Override
    public void process(Player player, String[] args) {
        if (args.length == 0) {
            player.sendMessage("§c/gamemode <GameMode> [Player]");
        } else if (args.length == 1) {
            GameMode gameMode = getGameMode(args[0]);
            player.sendMessage("§eYour game mode has been set to " + gameMode + "!");
            player.getBukkitPlayer().setGameMode(gameMode);
        } else if (args.length == 2) {
            GameMode gameMode = getGameMode(args[0]);

            if (!playerProvided(player, 1, args))
                return;

            Player target = Testserver.getInstance().getPlayerManager().getPlayer(args[1]);
            target.sendMessage("§eYour game mode has been set to " + gameMode + "!");
            player.sendMessage("§eYou have set " + target.getNameWithPrefix() + "§e's game mode to " + gameMode + "!");
            target.getBukkitPlayer().setGameMode(gameMode);
        }
    }

    private GameMode getGameMode(String arg) {
        GameMode gameMode = GameMode.SURVIVAL;
        if (arg.equalsIgnoreCase("survival") || arg.equalsIgnoreCase("s") ||
            arg.equalsIgnoreCase("0"))
            gameMode = GameMode.SURVIVAL;
        else if (arg.equalsIgnoreCase("creative") || arg.equalsIgnoreCase("c") ||
                 arg.equalsIgnoreCase("1"))
            gameMode = GameMode.CREATIVE;
        else if (arg.equalsIgnoreCase("adventure") || arg.equalsIgnoreCase("a") ||
                 arg.equalsIgnoreCase("2"))
            gameMode = GameMode.ADVENTURE;
        else if (arg.equalsIgnoreCase("spectator") || arg.equalsIgnoreCase("sp") ||
                 arg.equalsIgnoreCase("3"))
            gameMode = GameMode.SPECTATOR;

        return gameMode;
    }
}
