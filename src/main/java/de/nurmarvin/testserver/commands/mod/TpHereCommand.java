package de.nurmarvin.testserver.commands.mod;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.command.condition.RankCondition;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;

import java.util.List;

public class TpHereCommand extends Command {
    public TpHereCommand() {
        super("tphere");
        this.addCondition(new RankCondition(Rank.MOD));
    }

    @Override
    public void process(Player player, String[] args) {
        if (args.length < 1) {
            player.sendMessage("§c/tphere <Player>");
            return;
        }

        if (!this.playerProvided(player, 0, args)) {
            player.sendMessage("§cThis player is not online!");
            return;
        }

        Player target = Testserver.getInstance().getPlayerManager().getPlayer(args[0]);

        target.getBukkitPlayer().teleport(player.getLocation());
        player.sendMessage("§eYou have teleported " + target.getName() + " to you.");
    }

    @Override
    public List<String> suggest(Player player, String[] args) {
        if (args.length == 1) {
            return getPlayerSuggestions(args[0]);
        }
        return super.suggest(player, args);
    }
}
