package de.nurmarvin.testserver.commands.admin.rank;

import com.google.common.collect.ImmutableList;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RankBelowCommand extends Command {
    public RankBelowCommand() {
        super("below");
    }

    @Override
    public void process(Player player, String[] args) {
        Rank rank = Rank.getRank(args[0]);

        if (rank == null) {
            player.sendMessage("§cUnknown rank! Availabe ranks:");
            player.sendMessage("§c" + Arrays.toString(Rank.values()));
            return;
        }

        if (!player.hasRank(rank, false)) {
            player.sendMessage("§cYou can't give players a higher rank as you have!");
            return;
        }

        int success = 0;

        for (Player players : player.getServer().getPlayers()) {
            if (players.hasRank(rank, false))
                continue;
            players.updateRank(rank);
            players.upload();
            success++;
        }

        player.sendMessage("§aGave " + rank.name() + " to " + success + " players (Skipped " +
                           (player.getServer().getPlayers().length - success) + " players)");
    }

    @Override
    public List<String> suggest(Player player, String[] args) {
        if (args.length == 1)
            return StringUtil.copyPartialMatches(args[0], Arrays.asList(Arrays.stream(Rank.values()).
                    map(Enum::name).toArray(String[]::new)), new ArrayList<>(Rank.values().length));
        return ImmutableList.of("");
    }
}
