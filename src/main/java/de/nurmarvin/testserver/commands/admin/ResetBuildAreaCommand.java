package de.nurmarvin.testserver.commands.admin;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.command.condition.RankCondition;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;
import de.nurmarvin.testserver.utils.BuildAreaRunnable;

public class ResetBuildAreaCommand extends Command {
    public ResetBuildAreaCommand() {
        super("resetbuildarea");
        this.addCondition(new RankCondition(Rank.ADMIN));
    }

    @Override
    public void process(Player player, String[] args) {
        Testserver.getInstance().runAsync(new BuildAreaRunnable());
    }
}
