package de.nurmarvin.testserver.commands.admin.rank;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.command.condition.RankCondition;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RankCommand extends Command {
    public RankCommand() {
        super("rank");
        this.addCondition(new RankCondition(Rank.ADMIN));
        this.addSubCommand(new RankServerCommand());
        this.addSubCommand(new RankBelowCommand());
    }

    @Override
    public void process(Player player, String[] args) {
        if (args.length < 2) {
            player.sendMessage("§c/rank <player|server|below> <Rank>");
        } else {
            if (Testserver.getInstance().getPlayerManager().getPlayer(args[0]) != null) {
                Rank rank = Rank.getRank(args[1]);

                if (rank == null) {
                    player.sendMessage("§cUnknown rank! Available ranks:");
                    player.sendMessage("§c" + Arrays.toString(Rank.values()));
                    return;
                }

                if (!player.hasRank(rank, false)) {
                    player.sendMessage("§cYou can't give players a higher rank as you have!");
                    return;
                }

                Player target = Testserver.getInstance().getPlayerManager().getPlayer(args[0]);

                /*if(target.hasRank(player.getRank(), false))
                {
                    player.sendMessage("§cYou can't change ranks of players with the same or higher rank that you
                    have!");
                    return;
                }*/

                target.updateRank(rank);
                target.upload();

                player.sendMessage("§aGave " + rank.name() + " to " + target.getName());
            } else {
                super.execute(player, args);
            }
        }
    }

    @Override
    public List<String> suggest(Player player, String[] args) {
        if (args.length == 2) {
            return StringUtil.copyPartialMatches(args[1], Arrays.asList(Arrays.stream(Rank.values())
                                                                              .map(Enum::name).toArray(String[]::new)),
                                                 new ArrayList<>(Rank.values().length));
        }
        List<String> strings = super.suggest(player, args);

        if (args.length == 1)
            strings.addAll(getPlayerSuggestions(args[0]));

        return strings;
    }
}
