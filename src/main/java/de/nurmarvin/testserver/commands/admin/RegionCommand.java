package de.nurmarvin.testserver.commands.admin;

import com.google.common.collect.ImmutableList;
import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.region.Cuboid;
import de.nurmarvin.testserver.region.Region;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class RegionCommand extends Command {

    private static final List<String> SUB_COMMANDS = ImmutableList.of("set", "remove", "tp", "list");

    public RegionCommand() {
        super("region");
    }

    @Override
    public void process(Player player, String[] args) {
        if (args.length == 0 || args.length > 2)
            player.sendMessage("§c/region <set, remove, tp, list> [Region]");
        else if (args.length == 1) {
            if (args[0].equalsIgnoreCase("list")) {
                StringBuilder stringBuilder = new StringBuilder();

                int i = 0;
                for (Region region : Testserver.getInstance().getRegionManager().getRegions().values()) {
                    if (i != 0)
                        stringBuilder.append(", ");
                    stringBuilder.append(region.getName());
                    i++;
                }

                player.sendMessage("§7Regions: " + stringBuilder.toString());
                return;
            }
            if (SUB_COMMANDS.contains(args[0].toLowerCase()))
                player.sendMessage("§c/region " + args[0] + " <Region>");
            else
                player.sendMessage("§c/region <set|remove|tp|list> [Region]");
        } else if (args.length == 2)
            switch (args[0].toLowerCase()) {
                case "set": {
                    if (player.getSelection().getPos1() == null || player.getSelection().getPos2() == null) {
                        player.sendMessage("§cYou haven't made a selection yet!");
                        return;
                    }

                    if (Testserver.getInstance().getRegionManager().getRegions().containsKey(args[1].toLowerCase()))
                        Testserver.getInstance().getRegionManager().getRegions().remove(args[1].toLowerCase());

                    Testserver.getInstance().getRegionManager().getRegions().put(args[1].toLowerCase(),
                                                                                 new Region(args[1], new Cuboid(
                                                                                         player.getSelection()
                                                                                               .getPos1(),
                                                                                         player.getSelection()
                                                                                               .getPos2())));
                    player.sendMessage("§eThe region §a" + args[1] + "§e was successfully created!");
                    break;
                }
                case "remove": {
                    if (Testserver.getInstance().getRegionManager().getRegions().containsKey(args[1].toLowerCase())) {
                        player.sendMessage("§eThe region §a" + args[1] + "§e was successfully removed!");
                        Testserver.getInstance().getRegionManager().getRegions().remove(args[1].toLowerCase());
                    } else {
                        player.sendMessage("§cThis region doesn't exist!");
                    }
                    break;
                }
                case "tp": {
                    if (Testserver.getInstance().getRegionManager().getRegions().containsKey(args[1].toLowerCase())) {
                        player.sendMessage("§eYou were teleported to the region §a" + args[1] + "§e!");
                        player.getBukkitPlayer().teleport(
                                Testserver.getInstance().getRegionManager().getRegions().get(args[1].toLowerCase())
                                          .getCuboid().getUpperSW());
                    } else {
                        player.sendMessage("§cThis region doesn't exist!");
                    }
                    break;
                }
            }
    }

    @Override
    public List<String> suggest(Player player, String[] args) {
        if (args.length == 1) {
            return StringUtil.copyPartialMatches(args[0], SUB_COMMANDS, new ArrayList<>(SUB_COMMANDS.size()));
        } else if (args.length == 2) {
            return StringUtil
                    .copyPartialMatches(args[0], Testserver.getInstance().getRegionManager().getRegions().keySet()
                            , new ArrayList<>(Testserver.getInstance().getRegionManager().getRegions().size()));
        }
        return ImmutableList.of();
    }
}
