package de.nurmarvin.testserver.commands.admin;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.command.condition.RankCondition;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;

public class DebugCommand extends Command {
    public DebugCommand() {
        super("debug");
        this.addCondition(new RankCondition(Rank.OWNER));
    }

    @Override
    public void process(Player player, String[] args) {
        Testserver.getInstance().setDebug(!Testserver.getInstance().isDebug());

        Testserver.getInstance().getServer().broadcastMessage("§c§l[DEBUG] §e" +
                                                              (Testserver.getInstance().isDebug() ? "Enabled" :
                                                               "Disabled") + " debug mode.");
    }
}
