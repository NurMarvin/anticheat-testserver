package de.nurmarvin.testserver.commands.admin;

import com.google.common.collect.ImmutableList;
import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.entities.Player;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LocationCommand extends Command {

    private static final List<String> SUB_COMMANDS = ImmutableList.of("set", "remove", "tp", "list");

    public LocationCommand() {
        super("location");
    }

    @Override
    public void process(Player player, String[] args) {
        if (args.length == 0 || args.length > 2)
            player.sendMessage("§c/location <set, remove, tp, list> [Location]");
        else if (args.length == 1) {
            if (args[0].equalsIgnoreCase("list")) {
                player.sendMessage("§7Locations: " + Arrays.toString(Testserver.getInstance().getLocationManager()
                                                                               .getLocations().keySet().toArray()));
                return;
            }
            if (SUB_COMMANDS.contains(args[0].toLowerCase()))
                player.sendMessage("§c/location " + args[0] + " <Region>");
            else
                player.sendMessage("§c/location <set|remove|tp|list> [Region]");
        } else if (args.length == 2)
            switch (args[0].toLowerCase()) {
                case "set": {
                    if (Testserver.getInstance().getLocationManager().getLocation(args[1].toLowerCase()) != null)
                        Testserver.getInstance().getRegionManager().getRegions().remove(args[1].toLowerCase());

                    Testserver.getInstance().getLocationManager().getLocations().put(args[1].toLowerCase(),
                                                                                     player.getLocation());
                    player.sendMessage("§eThe location §a" + args[1] + "§e was successfully created!");
                    break;
                }
                case "remove": {
                    if (Testserver.getInstance().getLocationManager().getLocation(args[1].toLowerCase()) != null) {
                        player.sendMessage("§eThe location §a" + args[1] + "§e was successfully removed!");
                        Testserver.getInstance().getLocationManager().getLocations().remove(args[1].toLowerCase());
                    } else {
                        player.sendMessage("§cThis location doesn't exist!");
                    }
                    break;
                }
                case "tp": {
                    if (Testserver.getInstance().getRegionManager().getRegions().containsKey(args[1].toLowerCase())) {
                        player.sendMessage("§eYou were teleported to the location §a" + args[1] + "§e!");
                        player.getBukkitPlayer().teleport(
                                Testserver.getInstance().getLocationManager().getLocation(args[1].toLowerCase()));
                    } else {
                        player.sendMessage("§cThis location doesn't exist!");
                    }
                    break;
                }
            }
    }

    @Override
    public List<String> suggest(Player player, String[] args) {
        if (args.length == 1) {
            return StringUtil.copyPartialMatches(args[0], SUB_COMMANDS, new ArrayList<>(SUB_COMMANDS.size()));
        } else if (args.length == 2) {
            return StringUtil
                    .copyPartialMatches(args[0], Testserver.getInstance().getLocationManager().getLocations().keySet()
                            , new ArrayList<>(Testserver.getInstance().getRegionManager().getRegions().size()));
        }
        return ImmutableList.of();
    }
}
