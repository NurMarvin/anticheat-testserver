package de.nurmarvin.testserver.commands.admin.rank;

import com.google.common.collect.ImmutableList;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RankServerCommand extends Command {
    public RankServerCommand() {
        super("server");
    }

    @Override
    public void process(Player player, String[] args) {
        Rank rank = Rank.getRank(args[0]);

        if (rank == null) {
            player.sendMessage("§cUnknown rank! Available ranks:");
            player.sendMessage("§c" + Arrays.toString(Rank.values()));
            return;
        }

        if (!player.hasRank(rank, false)) {
            player.sendMessage("§cYou can't give yourself a higher rank than you have yourself!");
            return;
        }

        for (Player players : player.getServer().getPlayers()) {
            players.updateRank(rank);
            players.upload();
        }

        player.sendMessage("§aGave " + rank.name() + " to " + player.getServer().getPlayers().length + " players");
    }

    @Override
    public List<String> suggest(Player player, String[] args) {
        if (args.length == 1)
            return StringUtil.copyPartialMatches(args[0], Arrays.asList(
                    Arrays.stream(Rank.values()).map(Enum::name).toArray(String[]::new)),
                                                 new ArrayList<>(Rank.values().length));
        return ImmutableList.of("");
    }
}
