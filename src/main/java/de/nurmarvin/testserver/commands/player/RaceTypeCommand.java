package de.nurmarvin.testserver.commands.player;

import com.google.common.collect.Lists;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.race.RaceType;
import org.bukkit.util.StringUtil;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RaceTypeCommand extends Command {
    public RaceTypeCommand() {
        super("racetype");
    }

    @Override
    public void process(Player player, String[] args) {
        if (args.length < 1) {
            player.sendMessage("§c/racetype <RaceType>");
            return;
        }

        RaceType raceType = RaceType.getRaceType(args[0]);

        if (raceType == null) {
            player.sendMessage("§cUnknown race type! Available race types:");
            player.sendMessage("§c" + Arrays.toString(RaceType.values()));
            return;
        }

        player.setRaceType(raceType);
        player.sendMessage("§eYour race type was changed to " + raceType.toString().toUpperCase());
    }

    @Override
    public List<String> suggest(Player player, String[] args) {
        if (args.length == 1) {
            return StringUtil.copyPartialMatches(args[0], Arrays.stream(RaceType.values())
                                                                .map(Enum::name).collect(Collectors.toList()),
                                                 Lists.newArrayList());
        }
        return super.suggest(player, args);
    }
}
