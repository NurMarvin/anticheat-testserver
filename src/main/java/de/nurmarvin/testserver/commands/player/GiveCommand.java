package de.nurmarvin.testserver.commands.player;

import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GiveCommand extends Command {
    public GiveCommand() {
        super("give");
        this.addAlias("i");
        this.addAlias("item");
    }

    @Override
    public void process(Player player, String[] args) {
        if (args.length < 1) {
            player.sendMessage("§c/give <Item> [Amount]");
            return;
        }

        if (args.length == 1) {
            Material material = Material.matchMaterial(args[0]);

            if (material == null) {
                player.sendMessage("§cCan't find the a material by the name §e" + args[0]);
                return;
            }

            player.getInventory().addItem(new ItemStack(material));
        } else {
            Material material = Material.matchMaterial(args[0]);

            if (material == null) {
                player.sendMessage("§cCan't find the a material by the name §e" + args[0]);
                return;
            }

            int amount;


            try {
                amount = Integer.parseInt(args[1]);
            } catch (NumberFormatException exception) {
                amount = 1;
            }

            if (amount < 0)
                amount = -amount;

            if (amount > material.getMaxStackSize())
                if (!player.hasRank(Rank.MOD, false))
                    amount = material.getMaxStackSize();

            player.getInventory().addItem(new ItemStack(material, amount));
        }
    }

    @Override
    public List<String> suggest(Player player, String[] args) {
        if (args.length == 1) {
            return StringUtil.copyPartialMatches(args[0], Arrays.asList(Arrays.stream(Material.values())
                                                                              .map(Enum::name).toArray(String[]::new)),
                                                 new ArrayList<>(Material.values().length));
        }
        return super.suggest(player, args);
    }
}
