package de.nurmarvin.testserver.commands.player.tpa;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.command.CommandRegistry;
import de.nurmarvin.testserver.entities.Player;
import org.bukkit.ChatColor;

import java.util.List;

public class TpaCommand extends Command {
    public TpaCommand() {
        super("tpa");
        CommandRegistry.getInstance().registerCommand(new TpAcceptCommand());
        CommandRegistry.getInstance().registerCommand(new TpDenyCommand());
    }

    @Override
    public void process(Player player, String[] args) {
        if (args.length > 0) {
            if (!playerProvided(player, 0, args))
                return;
            Player target = Testserver.getInstance().getPlayerManager().getPlayer(args[0]);

            target.sendTeleportRequest(player);
            return;
        }
        super.process(player, args);
    }

    @Override
    public List<String> suggest(Player player, String[] args) {
        if (args.length == 1)
            return getPlayerSuggestions(args[0]);
        return super.suggest(player, args);
    }

    @Override
    public String describeUsage() {
        return ChatColor.RED + "/tpa <Player>";
    }
}
