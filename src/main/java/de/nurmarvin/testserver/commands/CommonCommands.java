package de.nurmarvin.testserver.commands;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.command.annotation.SimpleCommand;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.npcs.NPC;
import de.nurmarvin.testserver.permissions.Rank;
import de.nurmarvin.testserver.utils.KillPotionRunnable;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class CommonCommands {
    @SimpleCommand(name = "fly", requiredRank = Rank.DEV)
    public void commandFly(Player player, String[] args) {
        player.getBukkitPlayer().setAllowFlight(!player.getBukkitPlayer().getAllowFlight());
        if (player.getBukkitPlayer().isFlying()) player.getBukkitPlayer().setFlying(false);

        player.sendMessage("§eYou can " + (player.getBukkitPlayer().getAllowFlight() ? "now" : "no longer") + " fly!");
    }

    @SimpleCommand(name = "killall", requiredRank = Rank.ADMIN)
    public void commandKillAll(Player player, String[] args) {
        Testserver.getInstance().runAsync(new KillPotionRunnable());
        player.sendMessage("§eKilling initialized!");
    }

    @SimpleCommand(name = "heal", requiredRank = Rank.USER)
    public void commandHeal(Player player, String[] args) {
        player.getBukkitPlayer().setHealth(20D);
        player.getBukkitPlayer().setFoodLevel(20);
        player.sendMessage("§eYou have been healed!");
    }

    @SimpleCommand(name = "damage", requiredRank = Rank.USER)
    public void commandDamage(Player player, String[] args) {
        player.setDamage(!player.getDamage());
        player.sendMessage("§eYou have " + (player.getDamage() ? "§aenabled" : "§cdisabled") + " §edamage!");
    }

    @SimpleCommand(name = "setbacks", requiredRank = Rank.USER)
    public void commandSetBacks(Player player, String[] args) {
        player.setSetBacks(!player.getSetBacks());
        player.sendMessage("§eYou have " + (player.getSetBacks() ? "§aenabled" : "§cdisabled") + " §eset backs!");
    }

    @SimpleCommand(name = "kicks", requiredRank = Rank.USER)
    public void commandKicks(Player player, String[] args) {
        player.setKicks(!player.getKicks());
        player.sendMessage("§eYou have " + (player.getKicks() ? "§aenabled" : "§cdisabled") + " §ekicks!");
    }

    @SimpleCommand(name = "hunger", requiredRank = Rank.USER)
    public void commandHunger(Player player, String[] args) {
        player.setHunger(!player.getHunger());
        player.sendMessage("§eYou have " + (player.getHunger() ? "§aenabled" : "§cdisabled") + " §ehunger!");
    }

    @SimpleCommand(name = "setslots", requiredRank = Rank.ADMIN)
    public void commandSetSlots(Player player, String[] args) {
        if (args.length < 1) {
            player.sendMessage("§c/setslots <Slots>");
            return;
        }

        int slots;

        try {
            slots = Integer.parseInt(args[0]);
        } catch (Exception ex) {
            player.sendMessage("§cSlots must be a number!");
            return;
        }

        try {
            Object playerList = Bukkit.getServer().getClass().getDeclaredMethod("getHandle").invoke(Bukkit.getServer());
            Field maxPlayersField = playerList.getClass().getSuperclass().getDeclaredField("maxPlayers");
            maxPlayersField.setAccessible(true);
            maxPlayersField.set(playerList, slots);
            player.sendMessage("§eYou have set the slots to " + slots + "!");
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    @SimpleCommand(name = "toggleborder", requiredRank = Rank.ADMIN)
    public void commandToggleBorder(Player player, String[] args) {
        Testserver.getInstance().getRealServer().setBorderEnabled(
                !Testserver.getInstance().getRealServer().isBorderEnabled());

        player.sendMessage(
                "§eThe border was " + (Testserver.getInstance().getRealServer().isBorderEnabled() ? "§aenabled"
                                                                                                  : "§cdisabled"));
    }

    @SimpleCommand(name = "spawnhologram", requiredRank = Rank.ADMIN)
    public void commandSpawnHologram(Player player, String[] args) {
        if (args.length < 1) {
            player.sendMessage("§c/spawnhologram <Message...>");
            return;
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            if (i != 0)
                builder.append(" ");
            builder.append(args[i]);
        }

        ArmorStand armorStand =
                (ArmorStand) player.getLocation().getWorld().spawnEntity(player.getLocation(), EntityType.ARMOR_STAND);
        armorStand.setCustomName(ChatColor.translateAlternateColorCodes('&', builder.toString()));
        armorStand.setCustomNameVisible(true);
        armorStand.setGravity(false);
        armorStand.setVisible(false);

        player.sendMessage("§eHologram spawned.");
    }

    @SimpleCommand(name = "toggleweather", requiredRank = Rank.ADMIN)
    public void commandToggleWeather(Player player, String[] args) {
        Testserver.getInstance().getRealServer().setWeatherEnabled(
                !Testserver.getInstance().getRealServer().isWeatherEnabled());

        player.sendMessage(
                "§eThe weather was " + (Testserver.getInstance().getRealServer().isWeatherEnabled() ? "§aenabled"
                                                                                                    : "§cdisabled"));
    }

    /*@SimpleCommand(name = "testforitems", requiredRank = Rank.ADMIN)
    public void commandTestForItems(Player player, String[] args)
    {
        List<String> items = new ArrayList<>();
        for(Material material : Material.values())
        {
            try
            {
                player.getInventory().setItem(0, new ItemStack(material));
                items.add(material.toString());
                player.getInventory().setItem(0, new ItemStack(Material.AIR));
            }
            catch(NullPointerException e)
            {
                continue;
            }
        }

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < items.size(); i++)
        {
            if(i != 0)
                stringBuilder.append(", ");
            stringBuilder.append("Material." + items.get(i) + "\n");
        }

        System.out.println("{ " + stringBuilder.toString() + " }");
    }*/

    @SimpleCommand(name = "npctest", requiredRank = Rank.ADMIN)
    public void commandNPCTest(Player player, String[] args) {
        if (args.length < 1) {
            player.sendMessage("§c/npc <Name>");
            return;
        }
        NPC npc = new NPC(args[0], player.getLocation());

        npc.create();
        for (Player players : Testserver.getInstance().getRealServer().getPlayers())
            npc.spawn(players);
    }

    @SimpleCommand(name = "flyspeed", requiredRank = Rank.MOD)
    public void commandFlySpeed(Player player, String[] args) {
        if (args.length < 1) {
            player.sendMessage("§c/flyspeed <Speed>");
            return;
        }

        int flySpeed;

        try {
            flySpeed = Integer.parseInt(args[0]);
        } catch (NumberFormatException exception) {
            flySpeed = 1;
        }

        if (flySpeed < 0)
            flySpeed = 0;

        if (flySpeed > 10)
            flySpeed = 10;

        float realFlySpeed = flySpeed / 10F;

        player.getBukkitPlayer().setFlySpeed(realFlySpeed);
    }
}
