package de.nurmarvin.testserver.velocity;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.utils.ActionBar;
import org.bukkit.util.Vector;

public class VelocityTest implements Runnable {
    @Override
    public void run() {
        for (Player player : Testserver.getInstance().getRealServer().getPlayers()) {
            if (Testserver.getInstance().getRegionManager().isInRegion(player, "velocity")) {
                double multiplier = Testserver.getInstance().getRegionManager().getRegion("velocity")
                                              .getCuboid().getCenter().distance(player.getLocation())  / 5;

                Vector vector = player.getLocation().toVector()
                                      .subtract(Testserver.getInstance().getRegionManager().getRegion("velocity")
                                                          .getCuboid().getCenter().toVector()).normalize()
                                      .multiply(multiplier).setY(0);

                player.getBukkitPlayer().setVelocity(vector);

                player.sendActionBar(String.format("§cExpected Velocity: x=%f, z=%f",
                                                   vector.getX(), vector.getZ()));
            }
        }
    }
}
