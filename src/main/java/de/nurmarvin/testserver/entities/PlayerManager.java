package de.nurmarvin.testserver.entities;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import de.nurmarvin.testserver.permissions.Rank;
import de.nurmarvin.testserver.race.RaceType;
import lombok.Data;
import org.bukkit.Bukkit;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Data
public class PlayerManager {
    private HashMap<UUID, Player> playerUuidCache;
    private HashMap<String, Player> playerNameCache;
    private Dao<Player, UUID> playerDao;

    public PlayerManager() {
        this.playerUuidCache = new HashMap<>();
        this.playerNameCache = new HashMap<>();
    }

    public Player getPlayer(UUID uuid) {
        if (this.playerUuidCache.containsKey(uuid)) {
            Player player = this.playerUuidCache.get(uuid);
            if (player != null && player.getBukkitPlayer() == null) {
                player.setBukkitPlayer(Bukkit.getPlayer(uuid));
            }
            return player;
        }
        QueryBuilder<Player, UUID> statementBuilder = this.playerDao.queryBuilder();
        try {
            statementBuilder.where().idEq(uuid);
            List<Player> players = this.playerDao.query(statementBuilder.prepare());
            Player player;
            if (players.size() > 0) {
                player = players.get(0);
            } else {
                if (Bukkit.getPlayer(uuid) != null) {
                    player = new Player(Bukkit.getPlayer(uuid), Rank.USER.toString(), true, false,
                                        true, false, RaceType.NORMAL);
                    player.upload();
                } else
                    player = null;
            }

            if (player != null && player.getBukkitPlayer() == null) {
                player.setBukkitPlayer(Bukkit.getPlayer(uuid));
            }

            if (player != null)
                this.playerUuidCache.put(uuid, player);

            return player;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Player getPlayer(String name) {
        if (this.playerNameCache.containsKey(name))
            return this.playerNameCache.get(name);
        QueryBuilder<Player, UUID> statementBuilder = this.playerDao.queryBuilder();
        try {
            statementBuilder.where().eq("name", name);
            List<Player> players = this.playerDao.query(statementBuilder.prepare());
            Player player;
            if (players.size() > 0) {
                player = players.get(0);
            } else {
                if (Bukkit.getPlayerExact(name) != null) {
                    player = new Player(Bukkit.getPlayerExact(name), Rank.USER.toString(), true, false,
                                        true, false, RaceType.NORMAL);
                    player.upload();
                } else
                    player = null;
            }

            if (player != null && player.getBukkitPlayer() == null) {
                player.setBukkitPlayer(Bukkit.getPlayerExact(name));
            }
            if (player != null)
                this.playerNameCache.put(name, player);

            return player;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void uploadPlayer(Player player) {
        try {
            this.playerDao.createOrUpdate(player);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
