package de.nurmarvin.testserver.entities;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.events.RankUpdateEvent;
import de.nurmarvin.testserver.permissions.Rank;
import de.nurmarvin.testserver.race.RaceType;
import de.nurmarvin.testserver.region.Selection;
import de.nurmarvin.testserver.utils.*;
import de.nurmarvin.testserver.utils.scoreboard.ScoreboardUtil;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.inventory.PlayerInventory;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@DatabaseTable(tableName = "players")
public class Player {
    @DatabaseField(dataType = DataType.UUID, id = true)
    private UUID uuid;
    @DatabaseField
    private String name;
    @DatabaseField
    private Rank rank;
    @DatabaseField
    private Boolean damage;
    @DatabaseField
    private Boolean hunger;
    @DatabaseField
    private Boolean setBacks;
    @DatabaseField
    private Boolean kicks;
    @DatabaseField
    private RaceType raceType;
    @DatabaseField
    private boolean banned;
    @DatabaseField
    private String banReason;
    @DatabaseField
    private boolean muted;
    @DatabaseField
    private String muteReason;


    private Boolean canBuild;

    private org.bukkit.entity.Player bukkitPlayer;

    private List<UUID> tpaRequests;
    private Location lastLocation;
    private Selection selection;

    private Scroller scoreboardScroller;

    public Player(org.bukkit.entity.Player bukkitPlayer, String rank, boolean damage, boolean hunger, boolean setBacks,
                  boolean kicks, RaceType raceType) {
        this();
        this.bukkitPlayer = bukkitPlayer;
        this.name = bukkitPlayer.getName();
        this.uuid = bukkitPlayer.getUniqueId();
        this.rank = Rank.getRank(rank);
        this.damage = damage;
        this.hunger = hunger;
        this.setBacks = setBacks;
        this.kicks = kicks;
        this.raceType = raceType;
        this.banned = false;
        this.muted = false;
    }

    public Player() {
        selection = new Selection();
        this.tpaRequests = new ArrayList<>();
        this.canBuild = false;
    }

    public boolean hasRank(Rank rank) {
        return this.hasRank(rank, true);
    }

    public boolean hasRank(Rank rank, boolean inform) {
        return this.rank.has(this, rank, inform);
    }

    public void sendMessage(String message) {
        getBukkitPlayer().sendMessage(message);
    }

    public Object getHandle() {
        try {
            return this.bukkitPlayer.getClass().getDeclaredMethod("getHandle")
                                    .invoke(this.bukkitPlayer);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Object getPlayerConnection() {
        try {
            return this.getHandle().getClass().getField("playerConnection");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        return null;
    }

    public PlayerInventory getInventory() {
        return this.getBukkitPlayer().getInventory();
    }

    public void sendTabList(String header, String footer) {
        TabList tabList = new TabList(header, footer);
        tabList.send(this);
    }

    public void sendActionBar(String message) {
        ActionBar actionBar = new ActionBar(message);
        actionBar.send(this);
    }

    public void sendPacket(Object packet) {
        try {
            this.getPlayerConnection().getClass().getMethod("sendPacket",
                                                            ReflectionUtils.getRefClass("Packet").getRealClass())
                .invoke(this.getPlayerConnection(), packet);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getPing() {
        try {
            return this.getHandle().getClass().getDeclaredField("ping").getInt(this.getHandle());
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public Location getLocation() {
        return this.getBukkitPlayer().getLocation().clone();
    }

    /*public void openBook(ItemStack book) {
        int slot = this.getInventory().getHeldItemSlot();
        ItemStack old = this.getInventory().getItem(slot);
        this.getInventory().setItem(slot, book);

        ByteBuf buf = Unpooled.buffer(256);
        buf.setByte(0, (byte)0);
        buf.writerIndex(1);

        PacketPlayOutCustomPayload packet = new PacketPlayOutCustomPayload("MC|BOpen", new PacketDataSerializer(buf));
        this.getPlayerConnection().sendPacket(packet);
        this.getInventory().setItem(slot, old);
    }*/

    public String getName() {
        return /*this.getNickManager().isNicked() ? this.getNickManager().getNickname() : */this.name;
    }

    /*public Rank getRankConsideringNick()
    {
        return this.getNickManager().isNicked() ? this.getNickManager().getRank() : this.rank;
    }*/

    public void updateRank(Rank rank) {
        RankUpdateEvent rankUpdateEvent = new RankUpdateEvent(this, this.rank, rank);
        this.setRank(rank);

        Testserver.getInstance().getServer().getScheduler().runTaskLater(Testserver.getInstance(), () ->
                Bukkit.getPluginManager().callEvent(rankUpdateEvent), 5);
        this.sendMessage("§aYou rank was updated to " + rank.name());
    }

    public void sendTeleportRequest(Player player) {
        if (tpaRequests.contains(player.getUuid())) {
            player.sendMessage("§cYou already have sent this player a teleport request.");
            return;
        }

        this.tpaRequests.add(player.getUuid());
        player.sendMessage("§eYou have sent " + this.getNameWithPrefix() + " §ea teleport request.");
        this.sendMessage(player.getNameWithPrefix() + " §ehas sent you a teleport request.");
    }

    public void acceptTeleportRequest(Player player) {
        if (!tpaRequests.contains(player.getUuid())) {
            player.sendMessage("§cThis player hasn't sent you a teleport request.");
            return;
        }

        player.sendMessage(this.getNameWithPrefix() + " §ehas accepted your teleport request.");
        this.sendMessage("§eYou have accepted " + player.getNameWithPrefix() + "§e's teleport request.");
        player.getBukkitPlayer().teleport(this.getBukkitPlayer().getLocation());
        this.tpaRequests.remove(player.getUuid());
    }

    public void denyTeleportRequest(Player player) {
        if (!tpaRequests.contains(player.getUuid())) {
            player.sendMessage("§cThis player hasn't sent you a teleport request.");
            return;
        }

        player.sendMessage(this.getNameWithPrefix() + " §ehas denied your teleport request.");
        this.sendMessage("§eYou have denied " + player.getNameWithPrefix() + "§e's teleport request.");
        this.tpaRequests.remove(player.getUuid());
    }

    public void sendScoreboard() {
        if (scoreboardScroller == null)
            scoreboardScroller = new Scroller("§aNurMarvin's §eModern §6Anti Cheat §cTest Server", 16,
                                              5, '§');

        ScoreboardUtil.unrankedSidebarDisplay(this.getBukkitPlayer(),
                                              scoreboardScroller.next().replace("'", "\'"),
                                              "§7Ping: §7" + this.getPing(),
                                              "§7Rank: " + this.getRank().getName(),
                                              "§1",
                                              "§7Damage: " + (this.getDamage() ? "§aon" : "§coff"),
                                              "§7Hunger: " + (this.getHunger() ? "§aon" : "§coff"),
                                              "§7Setbacks: " + (this.getSetBacks() ? "§aon" : "§coff"),
                                              "§7Kicks: " + (this.getKicks() ? "§aon" : "§coff"));
    }

    public void upload() {
        Testserver.getInstance().getPlayerManager().uploadPlayer(this);
    }

    public Server getServer() {
        return Testserver.getInstance().getRealServer();
    }

    public String getNameWithPrefix() {
        return (/*this.getNickManager().isNicked() ? this.getNickManager().getRank() : */this.getRank()).getPrefix() +
               this.getName();
    }
}
