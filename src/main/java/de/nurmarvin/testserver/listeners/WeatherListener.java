package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WeatherListener implements Listener {
    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        event.setCancelled(!Testserver.getInstance().getRealServer().isWeatherEnabled());
        Testserver.debug("Weather change cancelled due to server settings");
    }
}
