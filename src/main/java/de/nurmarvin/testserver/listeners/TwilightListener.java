package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import me.vento.anticheat.events.api.PlayerThresholdEvent;
import me.vento.anticheat.events.api.PlayerViolationEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class TwilightListener implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onViolation(PlayerViolationEvent event) {
        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());

        player.sendMessage(String.format("§6§lTwilight§7 > %s failed §6%s §7category: %s " +
                                         "details: %s violations: " + event.getViolations() + " type: " +
                                         event.getName(),
                                         player.getNameWithPrefix(), event.getType().name(), event.getCategory(),
                                         event.getDetails()));

        if(!player.getSetBacks())
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onThreshold(PlayerThresholdEvent event) {
        double half = event.getViolation() >> 1;
        if(event.getViolation() == half) event.setCancelled(true);
        if(event.getViolation() == event.getThresholdViolation()) event.setCancelled(true);
    }
}
