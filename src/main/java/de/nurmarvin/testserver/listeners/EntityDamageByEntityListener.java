package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EntityDamageByEntityListener implements Listener {
    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof org.bukkit.entity.Player))
            return;

        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getEntity().getUniqueId());

        if (Testserver.getInstance().getRegionManager().isInRegion(player, "pvp")) {
            event.setCancelled(false);
            return;
        }

        event.setCancelled(true);
    }
}
