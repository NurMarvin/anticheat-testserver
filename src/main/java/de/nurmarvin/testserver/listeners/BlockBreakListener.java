package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());

        if (Testserver.getInstance().getRegionManager().getRegion("buildarea").getCuboid()
                      .containsIgnoreY(event.getBlock().getLocation())) {
            event.setCancelled(false);
            return;
        }

        if (!player.getCanBuild())
            event.setCancelled(true);

        if (event.getBlock().getType() == Material.BARRIER && player.hasRank(Rank.OWNER, false))
            event.setCancelled(true);
    }
}
