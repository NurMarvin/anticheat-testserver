package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.events.RegionExitEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class RegionExitListener implements Listener {
    @EventHandler
    public void onRegionExit(RegionExitEvent event) {
        if (!event.getRegion().getName().equals("map"))
            return;
        Player player = event.getPlayer();

        if (player.getCanBuild())
            return;

        if (!Testserver.getInstance().getRealServer().isBorderEnabled())
            return;

        if (event.getPlayer().getLocation().getY() < 2)
            return;

        if (player.getLastLocation() == null)
            return;

        double multiplier = Math.max(0.5, Math.pow(player.getLastLocation().distance(player.getLocation()) * 1.8, 2));

        event.setCancelled(true);
        player.getBukkitPlayer().setVelocity(event.getRegion().getCuboid().getCenter().toVector().
                subtract(player.getLocation().toVector()).normalize().multiply(multiplier).setY(-0.5));
    }
}
