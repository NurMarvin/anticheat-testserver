package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.command.CommandExecutable;
import de.nurmarvin.testserver.command.CommandRegistry;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandListener implements Listener {
    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());
        String commandName = event.getMessage().toLowerCase().substring(1);
        String[] args = new String[0];

        if (commandName.contains(" ")) {
            commandName = commandName.split(" ")[0];
            args = event.getMessage().substring(event.getMessage().indexOf(' ') + 1).split(" ");
        }

        if (!CommandRegistry.getInstance().getCommands().containsKey(commandName)) {
            if (commandName.equalsIgnoreCase("twilight"))
                return;
            if (!player.hasRank(Rank.ADMIN))
                event.setCancelled(true);
            return;
        }

        CommandExecutable command = CommandRegistry.getInstance().getCommands().get(commandName);

        if (command == null) {
            event.setCancelled(true);
            player.sendMessage("§cUnknown command. Type \"/help\" for a list of commands.");
            return;
        }

        command.execute(player, args);

        event.setCancelled(true);
    }
}
