package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.events.AsyncPlayerMoveEvent;
import de.nurmarvin.testserver.events.RegionEnterEvent;
import de.nurmarvin.testserver.events.RegionExitEvent;
import de.nurmarvin.testserver.region.Region;
import de.nurmarvin.testserver.region.RegionManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;

public class PlayerMoveListener implements Listener {
    @EventHandler
    public void onPlayerMove(AsyncPlayerMoveEvent event) {
        Player player = event.getPlayer();
        RegionManager regionManager = Testserver.getInstance().getRegionManager();

        if (regionManager.getPlayerInRegions().containsKey(player.getUuid())) {
            ArrayList<Region> regions = new ArrayList<>(regionManager.getPlayerInRegions().get(player.getUuid()));
            for (Region region : regions) {
                if (!region.getCuboid().contains(player.getLocation())) {
                    RegionExitEvent exitEvent =
                            new RegionExitEvent(player, region);
                    Bukkit.getServer().getPluginManager().callEvent(exitEvent);
                    event.setCancelled(exitEvent.isCancelled());

                    if (!event.isCancelled())
                        regionManager.exitRegion(player, region);
                }
            }
        }
        for (Region region : regionManager.getRegions().values()) {
            if (regionManager.getPlayerInRegions().containsKey(player.getUuid())
                && regionManager.getPlayerInRegions().get(player.getUuid()).contains(region))
                return;
            if (region.getCuboid().contains(player.getLocation())) {
                RegionEnterEvent enterEvent = new RegionEnterEvent(player, region);
                Bukkit.getServer().getPluginManager().callEvent(enterEvent);
                event.setCancelled(enterEvent.isCancelled());

                if (!event.isCancelled())
                    regionManager.enterRegion(player, region);
            }
        }
    }
}