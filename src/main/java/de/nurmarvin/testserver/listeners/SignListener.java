package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class SignListener implements Listener {
    @EventHandler
    public void onSignChange(SignChangeEvent event) {
        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());

        if (event.getLine(1) != null && event.getLine(1).equalsIgnoreCase("Warp")) {
            if (!player.hasRank(Rank.ADMIN))
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void onSignInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK) {
            if (event.getClickedBlock().getType() == Material.SIGN
                || event.getClickedBlock().getType() == Material.WALL_SIGN) {
                Sign sign = (Sign) event.getClickedBlock().getState();

                if (sign.getLine(1) != null && sign.getLine(2) != null && sign.getLine(1).equalsIgnoreCase("Warp")) {
                    Location location = Testserver.getInstance().getLocationManager()
                                                  .getLocation(sign.getLine(2).replaceAll(" ", "_"));

                    if (location == null) {
                        event.getPlayer().sendMessage("§eThe teleport location was not found, please report this to" +
                                                      " an admin!");
                        return;
                    }

                    event.getPlayer().teleport(location);
                }
            }
        }
    }
}
