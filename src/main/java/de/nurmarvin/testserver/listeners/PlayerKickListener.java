package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;

public class PlayerKickListener implements Listener {
    @EventHandler
    public void onKick(PlayerKickEvent event) {
        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());

        if (!player.getKicks()) {
            event.setCancelled(true);
            player.sendMessage("§cYou would have been kicked for: §e" + event.getReason());
        }
    }
}
