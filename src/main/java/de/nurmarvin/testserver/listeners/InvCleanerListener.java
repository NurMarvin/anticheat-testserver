package de.nurmarvin.testserver.listeners;

import com.google.common.collect.Maps;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.events.RegionEnterEvent;
import de.nurmarvin.testserver.events.RegionExitEvent;
import de.nurmarvin.testserver.utils.ItemStackUtils;
import de.nurmarvin.testserver.utils.UtilMath;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import java.util.TreeMap;
import java.util.UUID;

public class InvCleanerListener implements Listener {
    private TreeMap<UUID, TreeMap<Integer, ItemStack>> inventories;

    public InvCleanerListener() {
        this.inventories = Maps.newTreeMap();
    }

    @EventHandler
    public void onRegionEnter(RegionEnterEvent event) {
        Player player = event.getPlayer();
        if (event.getRegion().getName().startsWith("invcleaner")) {
            TreeMap<Integer, ItemStack> inventory = Maps.newTreeMap();

            for (int i = 0; i < player.getInventory().getSize(); i++) {
                ItemStack itemStack = player.getInventory().getItem(i);
                if (itemStack == null)
                    itemStack = new ItemStack(Material.AIR);
                inventory.put(i, itemStack);
            }

            for (int i = 0; i < 4; i++) {
                ItemStack itemStack = player.getInventory().getArmorContents()[i];
                if (itemStack == null)
                    itemStack = new ItemStack(Material.AIR);
                inventory.put(inventory.size() + 1, itemStack);
            }

            this.inventories.put(player.getUuid(), inventory);
            player.getInventory().clear();

            for (int i = 0; i < 36; i++) {
                ItemStack itemStack = null;

                while (itemStack == null)
                    itemStack = new ItemStack(ItemStackUtils.getItems()[UtilMath.r(ItemStackUtils.getItems().length)]);

                if (itemStack.getMaxStackSize() > 0)
                    itemStack.setAmount(UtilMath.r(itemStack.getMaxStackSize()));
                if (itemStack.getAmount() <= 0)
                    itemStack.setAmount(1);
                event.getPlayer().getInventory().setItem(i, itemStack);
            }
        }
    }

    @EventHandler
    public void onRegionExit(RegionExitEvent event) {
        Player player = event.getPlayer();
        if (event.getRegion().getName().startsWith("invcleaner")) {
            player.getInventory().clear();

            TreeMap<Integer, ItemStack> inventory = inventories.get(player.getUuid());

            for (int i = 0; i < 40; i++) {
                if (i > 36)
                    player.getInventory().getArmorContents()[i - 36] = inventory.get(i);
                if (inventory.get(i) != null)
                    player.getInventory().setItem(i, inventory.get(i));
            }
        }
    }
}
