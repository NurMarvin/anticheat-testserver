package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaceListener implements Listener {
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());

        if (Testserver.getInstance().getRegionManager().getRegion("buildarea").getCuboid()
                      .containsIgnoreY(event.getBlock().getLocation())) {
            event.setCancelled(false);
            return;
        }

        if (!player.getCanBuild())
            event.setCancelled(true);
    }
}
