package de.nurmarvin.testserver.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerListPingListener implements Listener {
    @EventHandler
    public void onServerPing(ServerListPingEvent event) {
        event.setMotd("§aNurMarvin's §eModern §6Anti Cheat §cTest Server\n" +
                      "§c§lNEW TESTS: §6Velocity §eInventory Cleaner");
    }
}
