package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.events.RankUpdateEvent;
import de.nurmarvin.testserver.utils.scoreboard.ScoreboardManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class RankUpdateListener implements Listener {
    @EventHandler
    public void onRankUpdate(RankUpdateEvent event) {
        ScoreboardManager.updateScoreboard(event.getPlayer());
    }
}
