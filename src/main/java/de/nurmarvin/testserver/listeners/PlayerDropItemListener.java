package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class PlayerDropItemListener implements Listener {
    @EventHandler(ignoreCancelled = true)
    public void onDrop(PlayerDropItemEvent event) {
        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());

        if (Testserver.getInstance().getRegionManager().isInRegionType(player, "invcleaner")) {
            event.getItemDrop().setPickupDelay(Integer.MAX_VALUE);
            Bukkit.getScheduler().runTaskLater(Testserver.getInstance(), () -> {
                event.getPlayer().getWorld().playSound(event.getItemDrop().getLocation(), Sound.ITEM_PICKUP, 1, 1);
                event.getItemDrop().remove();
            }, 20);
        }

    }
}
