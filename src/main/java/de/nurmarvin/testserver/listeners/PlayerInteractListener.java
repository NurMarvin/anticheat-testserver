package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;
import de.nurmarvin.testserver.region.Cuboid;
import de.nurmarvin.testserver.region.Selection;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener implements Listener {
    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK) {
            Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());

            if (event.getAction() == Action.RIGHT_CLICK_BLOCK)
                if (Testserver.getInstance().getRegionManager().getRegion("buildarea") != null)
                    if (!Testserver.getInstance().getRegionManager().getRegion("buildarea").getCuboid()
                                   .containsIgnoreY(event.getClickedBlock().getLocation()) && !player.getCanBuild())
                        event.setCancelled(true);

            if (event.getClickedBlock() != null && event.getClickedBlock().getType() != null
                && event.getClickedBlock().getType() == Material.WALL_SIGN) {
                Block sign = event.getClickedBlock();
                if (sign.getLocation().getBlockX() == -4 && sign.getLocation().getBlockZ() == 32) {
                    if (Testserver.getInstance().getRaceManager().getRace(player) != null) {
                        return;
                    }
                    if (sign.getLocation().getBlockY() == 65)
                        Testserver.getInstance().getRaceManager().joinSolo(player);
                    else
                        Testserver.getInstance().getRaceManager().joinDuo(player);
                }
            }

            if (player.hasRank(Rank.ADMIN, false)) {
                if (event.getPlayer().getItemInHand() != null &&
                    event.getPlayer().getItemInHand().getType() == Material.GOLD_AXE) {
                    event.setCancelled(true);
                    Selection selection = player.getSelection();

                    if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
                        selection.setPos1(event.getClickedBlock().getLocation());
                        player.sendMessage("§ePosition 1 set! (" + event.getClickedBlock().getX() + ", " +
                                           event.getClickedBlock().getY() + ", " + event.getClickedBlock().getZ() +
                                           ")");
                    } else {
                        selection.setPos2(event.getClickedBlock().getLocation());
                        player.sendMessage("§ePosition 2 set! (" + event.getClickedBlock().getX() + ", " +
                                           event.getClickedBlock().getY() + ", " + event.getClickedBlock().getZ() +
                                           ")");
                    }

                    if(selection.getPos1() != null && selection.getPos2() != null)
                    {
                        Cuboid cuboid = new Cuboid(selection.getPos1(), selection.getPos2());

                        Bukkit.getScheduler().runTaskLater(Testserver.getInstance(),
                                                           () -> cuboid.sendWallsAsFakeBlocks(player), 1L);
                    }
                }
            }
        }
    }
}
