package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class DamageListener implements Listener {
    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof org.bukkit.entity.Player))
            return;

        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getEntity().getUniqueId());

        if (event.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK)
            return;

        if (!player.getDamage()) {
            event.setCancelled(true);
        }
    }
}
