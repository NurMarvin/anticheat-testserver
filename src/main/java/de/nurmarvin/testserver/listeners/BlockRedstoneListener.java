package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;

public class BlockRedstoneListener implements Listener {
    @EventHandler
    public void onEntityExplode(BlockRedstoneEvent event) {
        if (Testserver.getInstance().getRegionManager().getRegion("buildarea").getCuboid()
                      .containsIgnoreY(event.getBlock().getLocation())
            && !Testserver.getInstance().getRealServer().isRedstoneEnabked())
            event.setNewCurrent(0);
    }
}
