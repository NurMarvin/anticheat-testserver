package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.StructureGrowEvent;

public class StructureGrowListener implements Listener {
    @EventHandler
    public void onStructureGrow(StructureGrowEvent event) {
        for (BlockState blocks : event.getBlocks())
            if (!Testserver.getInstance().getRegionManager().getRegion("buildarea").getCuboid()
                           .containsIgnoreY(blocks.getLocation()))
                blocks.setType(Material.AIR);
    }
}
