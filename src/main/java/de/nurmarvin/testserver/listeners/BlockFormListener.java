package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFormEvent;

public class BlockFormListener implements Listener {
    @EventHandler
    public void onBlockForm(BlockFormEvent event) {
        if (!Testserver.getInstance().getRegionManager().getRegion("buildarea").getCuboid()
                       .containsIgnoreY(event.getBlock().getLocation()))
            event.setCancelled(true);
    }
}
