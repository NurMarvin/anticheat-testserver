package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.events.RankUpdateEvent;
import de.nurmarvin.testserver.events.RegionEnterEvent;
import de.nurmarvin.testserver.events.RegionExitEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class DebugListeners implements Listener {
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onRegionEnter(RegionEnterEvent event) {
        Testserver.debug(event.toString());
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onRegionExit(RegionExitEvent event) {
        Testserver.debug(event.toString());
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onRankUpdate(RankUpdateEvent event) {
        Testserver.debug(event.toString());
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        Testserver.debug("Block with type " + event.getBlock().getType() + " was destroyed at"
                         + event.getBlock().getLocation().toString() + " by " + event.getPlayer().getName() +
                         "; cancelled=" + event.isCancelled());
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        Testserver.debug("Block with type " + event.getBlock().getType() + " was placed at"
                         + event.getBlock().getLocation().toString() + " by " + event.getPlayer().getName() +
                         "; cancelled=" + event.isCancelled());
    }
}
