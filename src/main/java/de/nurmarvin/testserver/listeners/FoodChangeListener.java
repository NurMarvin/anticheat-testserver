package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodChangeListener implements Listener {
    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        if (!(event.getEntity() instanceof org.bukkit.entity.Player))
            return;

        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getEntity().getUniqueId());

        if (!player.getHunger()) {
            event.setFoodLevel(20);
        }
    }
}
