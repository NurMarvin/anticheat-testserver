package de.nurmarvin.testserver.listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public class EntitySpawnListener implements Listener {
    @EventHandler
    public void onEntitySpawn(EntitySpawnEvent event) {
        if (event.getEntityType().equals(EntityType.ARMOR_STAND)
            || event.getEntityType().equals(EntityType.DROPPED_ITEM))
            return;
        event.setCancelled(true);
    }
}
