package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;

public class BlockFromToListener implements Listener {
    @EventHandler
    public void onBlockFromTo(BlockFromToEvent event) {
        if (!Testserver.getInstance().getRegionManager().getRegion("buildarea").getCuboid()
                       .containsIgnoreY(event.getToBlock().getLocation()))
            event.setCancelled(true);
    }
}
