package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {
    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());
        event.setCancelled(true);

        String colonColor = player.getRank().equals(Rank.USER) ? "§7" : "§f";

        String message = player.getNameWithPrefix() + colonColor + ": " + (player.hasRank(Rank.DEV) ?
                                                                           ChatColor.translateAlternateColorCodes('&',
                                                                                                                  event.getMessage()) :
                                                                           event.getMessage());

        for (Player players : player.getServer().getPlayers())
            players.sendMessage(message);

        Bukkit.getConsoleSender().sendMessage(message);
    }
}
