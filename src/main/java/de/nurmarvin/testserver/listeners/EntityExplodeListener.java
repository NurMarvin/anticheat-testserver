package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.Iterator;

public class EntityExplodeListener implements Listener {
    @EventHandler
    public void onEntityExplode(EntityExplodeEvent event) {
        for (Iterator<Block> block = event.blockList().iterator(); block.hasNext(); ) {
            if (!Testserver.getInstance().getRegionManager().getRegion("buildarea").getCuboid()
                           .containsIgnoreY(block.next().getLocation()))
                block.remove();
        }
    }
}
