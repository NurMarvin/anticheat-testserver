package de.nurmarvin.testserver.listeners;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.race.Race;
import de.nurmarvin.testserver.utils.scoreboard.ScoreboardManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerConnectionListener implements Listener {
    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());

        player.getBukkitPlayer().setHealth(20);
        player.getBukkitPlayer().setFoodLevel(20);

        player.sendScoreboard();

        ScoreboardManager.updateScoreboard(player);

        event.setJoinMessage(null);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());

        Race race = Testserver.getInstance().getRaceManager().getRace(player);

        if (race != null) {
            if (race.getPlayerOne() != null)
                race.sendMessage(
                        "§eThe player queued up with left the server so your race was converted to a solo race!");
            else
                Testserver.getInstance().getRaceManager().removeRace(race);
        }

        Testserver.getInstance().getPlayerManager().getPlayerNameCache().remove(event.getPlayer().getName());
        Testserver.getInstance().getPlayerManager().getPlayerUuidCache().remove(event.getPlayer().getUniqueId());

        event.setQuitMessage(null);
    }
}
