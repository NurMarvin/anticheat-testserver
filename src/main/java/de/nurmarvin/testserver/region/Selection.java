package de.nurmarvin.testserver.region;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bukkit.Location;

@Data
@NoArgsConstructor
public class Selection {
    private Location pos1;
    private Location pos2;
}
