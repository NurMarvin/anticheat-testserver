package de.nurmarvin.testserver.region;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Region {
    private String name;
    private Cuboid cuboid;
}
