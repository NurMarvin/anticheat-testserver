package de.nurmarvin.testserver.race;

import com.google.common.collect.Lists;
import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.events.RegionEnterEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.List;

public class RaceManager implements Listener {
    private Race currentRace;
    private List<Race> queue;
    private Race raceWaitingForSecondPlayer;

    public RaceManager() {
        this.currentRace = null;
        this.queue = Lists.newArrayList();
        Testserver.getInstance().registerListener(this);
    }

    public void joinSolo(Player player) {
        queue.add(new Race(player));

        if (currentRace != null) {
            player.sendMessage("§eQueued! You are position " + queue.size() + ".");
            return;
        }

        this.nextRace();
    }

    public void joinDuo(Player player) {
        if (raceWaitingForSecondPlayer != null) {
            this.raceWaitingForSecondPlayer.setPlayerTwo(player);
            queue.add(this.raceWaitingForSecondPlayer);

            if (currentRace == null) {
                this.nextRace();
                return;
            }

            this.raceWaitingForSecondPlayer
                    .sendMessage("§eQueued! You are position " + queue.size() + " with " + player.getName() + ".");
            this.raceWaitingForSecondPlayer = null;
            return;
        }

        this.raceWaitingForSecondPlayer
                .sendMessage("§eQueued! You are position " + queue.size() + " and have no partner yet.");

        this.raceWaitingForSecondPlayer = new Race(player, null);
    }

    public void nextRace() {
        if (this.queue.size() <= 0) {
            this.currentRace = null;
            return;
        }
        this.currentRace = queue.get(0);
        this.queue.remove(this.currentRace);

        this.currentRace.teleport();

        for (Race nextRaces : queue) {
            nextRaces.sendMessage("§eYou are now position " + queue.size());
        }

        this.currentRace.start();
    }

    public Race getRace(Player player) {
        if (this.raceWaitingForSecondPlayer != null &&
            (this.raceWaitingForSecondPlayer.getPlayerOne().getUuid() == player.getUuid()
             || this.raceWaitingForSecondPlayer.getPlayerTwo().getUuid() == player.getUuid()))
            return this.raceWaitingForSecondPlayer;
        if (this.currentRace != null && (this.currentRace.getPlayerOne().getUuid() == player.getUuid()
                                         || this.currentRace.getPlayerTwo().getUuid() == player.getUuid()))
            return this.currentRace;

        for (Race races : this.queue)
            if (races.getPlayerOne().getUuid() == player.getUuid()
                || races.getPlayerTwo().getUuid() == player.getUuid())
                return races;
        return null;
    }

    public void removeRace(Race race) {
        this.queue.remove(race);
    }

    @EventHandler
    public void onRegionEnter(RegionEnterEvent event) {
        if (event.getRegion().getName().equals("racefinish")) {
            Race race = this.getRace(event.getPlayer());

            if (race == null) {
                event.getPlayer()
                     .sendMessage("§eYou finished the race. Uh... I can't find you in the participant list\n" +
                                  "§eDid you really phase in here? c: If so, good job!");
                return;
            }

            race.finish(event.getPlayer());
        }
    }
}
