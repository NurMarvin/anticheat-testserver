package de.nurmarvin.testserver.race;

import lombok.Getter;

@Getter
public enum RaceType {
    NORMAL(8750),
    ICE(7000),
    SOUL_SAND(12000),
    ICE_SOUL_SAND(12000);

    private long vanillaTime;

    RaceType(long vanillaTime) {
        this.vanillaTime = vanillaTime;
    }

    public static RaceType getRaceType(String string) {
        for (RaceType raceType : values())
            if (raceType.toString().toLowerCase().equalsIgnoreCase(string.toLowerCase()))
                return raceType;
        return null;
    }
}
