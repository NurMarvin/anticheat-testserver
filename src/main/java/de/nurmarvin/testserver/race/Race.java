package de.nurmarvin.testserver.race;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.utils.UtilMath;
import de.nurmarvin.testserver.utils.UtilTime;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.List;

@Getter
@Setter
public class Race {
    private Player playerOne;
    private Player playerTwo;
    private long startTime;
    private RaceType raceType;

    public Race(Player playerOne) {
        this.startTime = -1;
        this.playerOne = playerOne;
        this.raceType = playerOne.getRaceType();
        this.playerTwo = null;
    }

    public Race(Player playerOne, Player playerTwo) {
        this.startTime = -1;
        this.playerOne = playerOne;
        this.raceType = playerOne.getRaceType();
        this.playerTwo = playerTwo;
    }

    public void start() {
        List<Block> blocks = Testserver.getInstance().getRegionManager()
                                       .getRegion("racefloor").getCuboid().getBlocks();

        switch (this.raceType) {
            case ICE: {
                for (Block block : blocks)
                    block.setType(Material.PACKED_ICE);
                break;
            }
            case NORMAL: {
                for (Block block : blocks) {
                    block.setType(Material.STAINED_CLAY);
                    block.setData((byte) 5);
                }
                break;
            }
            case SOUL_SAND: {
                for (Block block : blocks)
                    block.setType(Material.SOUL_SAND);
                break;
            }
            case ICE_SOUL_SAND: {
                for (Block block : blocks) {
                    block.setType(Material.SOUL_SAND);
                    Block blockBelow = block.getWorld().getBlockAt(block.getLocation().clone().subtract(0, 1, 0));
                    blockBelow.setType(Material.PACKED_ICE);
                }
                break;
            }
        }

        this.startTime = System.currentTimeMillis();
    }

    public void finish(Player player) {
        long finishTimeLong = System.currentTimeMillis() - this.startTime;
        double comparedToVanilla = ((this.raceType.getVanillaTime() * 1.0) / finishTimeLong) * 100;
        Testserver.debug("Compared to vanilla speed: " + comparedToVanilla);
        String finishTime = UtilTime.convertString(finishTimeLong, 2,
                                                   UtilTime.TimeUnit.SECONDS);

        String comparedString = "as fast as vanilla bunny hopping.";

        if (comparedToVanilla > 100)
            comparedString = "~" + UtilMath.trim(2, comparedToVanilla - 100) + " faster than vanilla bunny hopping.";
        else if (comparedToVanilla < 100)
            comparedString = "~" + UtilMath.trim(2, 100 - comparedToVanilla) + " slower than vanilla bunny hopping.";

        if (this.playerOne != null && this.playerOne.getUuid() == player.getUuid()) {
            this.playerOne.sendMessage("§eYou finished the race in " + finishTime + ".");
            this.playerOne.sendMessage("§eThat is " + comparedString);
            this.playerOne.getBukkitPlayer().teleport(this.playerOne.getLocation().getWorld().getSpawnLocation());
            this.playerOne = null;
            this.sendMessage("§ePlayer One finished the race in " + finishTime + "!");

        } else if (this.playerTwo != null && this.playerTwo.getUuid() == player.getUuid()) {
            this.playerTwo.sendMessage("§eYou finished the race in " + finishTime);
            this.playerTwo.sendMessage("§eThat is " + comparedString);
            this.playerTwo.getBukkitPlayer().teleport(this.playerTwo.getLocation().getWorld().getSpawnLocation());
            this.playerTwo = null;
            this.sendMessage("§ePlayer Two finished the race in " + finishTime + "!");
        }

        if (this.playerOne == null && this.playerTwo == null) {
            Testserver.getInstance().getRaceManager().nextRace();
        }
    }

    public void sendMessage(String message) {
        if (this.playerOne != null) {
            this.playerOne.sendMessage(message);
        }

        if (this.playerTwo != null) {
            this.playerTwo.sendMessage(message);
        }
    }

    public void teleport() {
        if (this.playerOne != null) {
            Location location = new Location(this.playerOne.getBukkitPlayer().getWorld(),
                                             -5.5, 63, 34.5);
            location.setYaw(90);
            location.setPitch(0);
            this.playerOne.getBukkitPlayer().teleport(location);
        }

        if (this.playerTwo != null) {
            Location location = new Location(this.playerTwo.getBukkitPlayer().getWorld(),
                                             -5.5, 63, 30.5);
            location.setYaw(90);
            location.setPitch(0);
            this.playerTwo.getBukkitPlayer().teleport(location);
        }
    }

    public void end() {

    }

}
