package de.nurmarvin.testserver;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.utility.MinecraftVersion;
import com.google.common.collect.Sets;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import de.nurmarvin.testserver.command.Command;
import de.nurmarvin.testserver.command.CommandRegistry;
import de.nurmarvin.testserver.commands.CommonCommands;
import de.nurmarvin.testserver.commands.admin.DebugCommand;
import de.nurmarvin.testserver.commands.admin.LocationCommand;
import de.nurmarvin.testserver.commands.admin.RegionCommand;
import de.nurmarvin.testserver.commands.admin.ResetBuildAreaCommand;
import de.nurmarvin.testserver.commands.admin.rank.RankCommand;
import de.nurmarvin.testserver.commands.mod.*;
import de.nurmarvin.testserver.commands.player.GiveCommand;
import de.nurmarvin.testserver.commands.player.RaceTypeCommand;
import de.nurmarvin.testserver.commands.player.tpa.TpaCommand;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.entities.PlayerManager;
import de.nurmarvin.testserver.events.AsyncPlayerMoveEvent;
import de.nurmarvin.testserver.listeners.*;
import de.nurmarvin.testserver.race.RaceManager;
import de.nurmarvin.testserver.region.RegionManager;
import de.nurmarvin.testserver.utils.*;
import de.nurmarvin.testserver.velocity.VelocityTest;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

@Getter
@Setter
public final class Testserver extends JavaPlugin {
    @Getter
    private static Testserver instance;
    private PlayerManager playerManager;
    private Server realServer;
    private ConnectionSource connectionSource;
    private FileManager fileManager;
    private RegionManager regionManager;
    private RaceManager raceManager;
    private LocationManager locationManager;
    private boolean debug;
    private ProtocolManager protocolManager;
    private Set<PacketType> packetsDenied = Sets.newLinkedHashSet();

    public static void debug(String message) {
        if (Testserver.getInstance().debug)
            Testserver.getInstance().getServer().broadcastMessage("§c§l[DEBUG] §e" + message);
    }

    @Override
    public void onLoad() {
        this.protocolManager = ProtocolLibrary.getProtocolManager();
    }

    @Override
    public void onEnable() {
        long start = System.currentTimeMillis();

        instance = this;

        this.fileManager = new FileManager(this);
        this.locationManager = new LocationManager();
        this.playerManager = new PlayerManager();

        String[] mysqlData = this.fileManager.read();

        if (mysqlData.length == 0) {
            System.err.println("Please make sure that you have provided MySQL credentials!");
            return;
        }

        try {
            connectionSource = new JdbcConnectionSource(mysqlData[0], mysqlData[1], mysqlData[2]);

            setupDatabase(connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.realServer = new Server();
        this.regionManager = new RegionManager();
        this.regionManager.loadRegions();

        this.raceManager = new RaceManager();

        this.registerCommand(new GameModeCommand());
        this.registerCommand(new RankCommand());
        this.registerCommand(new TpaCommand());
        this.registerCommand(new RegionCommand());
        this.registerCommand(new BuildCommand());
        this.registerCommand(new ResetBuildAreaCommand());
        this.registerCommand(new GiveCommand());
        this.registerCommand(new DebugCommand());
        this.registerCommand(new RaceTypeCommand());
        this.registerCommand(new TpCommand());
        this.registerCommand(new TpHereCommand());
        this.registerCommand(new LocationCommand());
        this.registerCommand(new BanCommand());
        CommandRegistry.getInstance().registerSimpleCommand(new CommonCommands(), "testserver");

        this.registerListener(new PlayerConnectionListener());
        this.registerListener(new EntitySpawnListener());
        this.registerListener(new RankUpdateListener());
        this.registerListener(new ChatListener());
        this.registerListener(new FoodChangeListener());
        this.registerListener(new DamageListener());
        this.registerListener(new CommandListener());
        this.registerListener(new PlayerInteractListener());
        this.registerListener(new BlockBreakListener());
        this.registerListener(new BlockPlaceListener());
        this.registerListener(new PlayerKickListener());
        this.registerListener(new BlockFromToListener());
        this.registerListener(new BlockRedstoneListener());
        this.registerListener(new EntityExplodeListener());
        this.registerListener(new ServerListPingListener());
        this.registerListener(new PlayerMoveListener());
        this.registerListener(new DebugListeners());
        this.registerListener(new RegionExitListener());
        this.registerListener(new PistonExtendListener());
        this.registerListener(new PistonRetractListener());
        this.registerListener(new EntityDamageByEntityListener());
        this.registerListener(new StructureGrowListener());
        this.registerListener(new PortalListener());
        this.registerListener(new InvCleanerListener());
        this.registerListener(new PlayerDropItemListener());
        this.registerListener(new WeatherListener());
        this.registerListener(new SignListener());
        this.registerListener(new TwilightListener());

        //NCPHookManager.addHook(CheckType.ALL, new NCPLogHook());

        this.getServer().getScheduler().runTaskTimerAsynchronously(this, () -> {
            for (Player player : this.getRealServer().getPlayers()) {
                this.runSync(player::sendScoreboard);
                Runtime runtime = Runtime.getRuntime();
                long freeMemory = runtime.freeMemory();
                long totalMemory = runtime.totalMemory();

                double ramUsedPercentage = UtilMath.trim(3, (totalMemory - freeMemory * 1.0) / totalMemory) * 100;

                player.sendTabList("§aNurMarvin\\'s §eModern §6Anti Cheat §cTest Server\n" +
                                   "§7Players: " + player.getServer().getPlayers().length + "/"
                                   + Testserver.getInstance().getServer().getMaxPlayers() + "\n",
                                   "\n§7TPS: " + player.getServer().getTPS() + "/20 " + UtilString
                                           .getBars(player.getServer().getTPS(), 20.0D, 10, false) + "\n" +
                                   "   §7RAM: " + String.valueOf(ramUsedPercentage).substring(0, 4) + "% " +
                                   UtilString.getBars(totalMemory - freeMemory, totalMemory,
                                                      10, true));
            }
        }, 0L, 4L);

        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new VelocityTest(), 0L, 1L);
        //this.getServer().getScheduler().runTaskTimerAsynchronously(this, new SnowEffectRunnable(), 0L, 10L);
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new BuildAreaRunnable(), 20L, 20L * 60 * 5);

        this.getServer().getScheduler().runTaskTimerAsynchronously(this, () ->
        {
            for (Player player : this.getRealServer().getPlayers()) {
                if (player.getBukkitPlayer() == null)
                    continue;
                if (player.getLocation() == null)
                    continue;
                if (player.getLastLocation() != null)
                    if (player.getLocation().getX() == player.getLastLocation().getX()
                        && player.getLocation().getY() == player.getLastLocation().getY()
                        && player.getLocation().getZ() == player.getLastLocation().getZ())
                        continue;
                AsyncPlayerMoveEvent
                        event = new AsyncPlayerMoveEvent(player, player.getLastLocation(), player.getLocation());
                this.getServer().getPluginManager().callEvent(event);

                if (event.isCancelled())
                    if (event.getFrom() != null)
                        event.getPlayer().getBukkitPlayer().teleport(event.getFrom());
                player.setLastLocation(player.getLocation());
            }
        }, 0, 1);

        packetsDenied = PacketType.Play.Server.getInstance().values();

        packetsDenied.remove(PacketType.Play.Server.LOGIN);

        /*for(PacketType packetType : packetsDenied)
            if(packetType.getCurrentVersion().isAtLeast(MinecraftVersion.COMBAT_UPDATE))
                packetsDenied.remove(packetType);*/

        protocolManager.addPacketListener(new PacketAdapter(this, ListenerPriority.NORMAL,
                                                            packetsDenied) {
            @Override
            public void onPacketSending(PacketEvent event) {
                Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());

                if (player == null)
                    return;

                if (player.isBanned()) {
                    debug("Cancelling " + event.getPacketType().name() + " for " + player.getNameWithPrefix());
                    event.setCancelled(true);
                }
            }
        });

        protocolManager.addPacketListener(new PacketAdapter(this, ListenerPriority.NORMAL,
                                                            PacketType.Play.Client.getInstance().values()) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                Player player = Testserver.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId());

                if (player == null)
                    return;

                if (player.isBanned()) {
                    debug("Cancelling " + event.getPacketType().name() + " for " + player.getNameWithPrefix());
                    event.setCancelled(true);
                }
            }
        });

        ConsoleCommandSender console = this.getServer().getConsoleSender();

        console.sendMessage("§e§m-------------------------------------------------------");
        console.sendMessage("");

        console.sendMessage("§aNurMarvin's §eModern §6Anti Cheat §cTest Server");
        console.sendMessage(String.format("with version %s was§a successfully§r enabled in %sms!",
                                          Testserver.getInstance().getDescription().getVersion(),
                                          System.currentTimeMillis() - start));

        console.sendMessage("");
        console.sendMessage("§e§m-------------------------------------------------------");
    }

    private void setupDatabase(ConnectionSource connectionSource) throws SQLException {

        playerManager.setPlayerDao(DaoManager.createDao(connectionSource, Player.class));

        TableUtils.createTableIfNotExists(connectionSource, Player.class);
    }

    public void runAsync(Runnable runnable) {
        getServer().getScheduler().runTaskAsynchronously(this, runnable);
    }

    public void runSync(Runnable runnable) {
        getServer().getScheduler().runTask(this, runnable);
    }

    public void registerListener(Listener listener) {
        this.getServer().getPluginManager().registerEvents(listener, this);
    }

    public void registerCommand(Command command) {
        CommandRegistry.getInstance().registerCommand(command, "twilight");
    }

    public void unregisterCommand(String command) {
        CommandRegistry.getInstance().getCommandMap().getCommand(command).
                unregister(CommandRegistry.getInstance().getCommandMap());
    }

    @Override
    public void onDisable() {
        this.regionManager.saveRegions();
        this.locationManager.saveLocations();
        try {
            this.connectionSource.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
