package de.nurmarvin.testserver.npcs;

import com.mojang.authlib.GameProfile;
import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.utils.ReflectionUtils;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

@Data
public class NPC {
    private final String name;
    private final Location location;
    private Object npc;

    public void create() {
        try {
            Object nmsServer = ReflectionUtils.getRefClass("{nms}.CraftServer")
                                              .getRealClass().cast(Bukkit.getServer());
            Object nmsWorld = location.getWorld().getClass().getMethod("getHandle").invoke(location.getWorld());

            npc = ReflectionUtils.getRefClass("{nms}.EntityPlayer").getConstructor().create(nmsServer, nmsWorld,
                                                                                            new GameProfile(
                                                                                                    UUID.randomUUID(),
                                                                                                    this.name));
            npc.getClass().getDeclaredMethod("setLocation").invoke(npc,
                                                                   location.getX(), location.getY(), location.getZ(),
                                                                   location.getYaw(), location.getPitch());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public void spawn(Player player) {
        Object packetPlayOutPlayerInfo = ReflectionUtils.getRefClass("{nms}.PacketPlayOutPlayerInfo")
                                                        .getConstructor().create(ReflectionUtils.getRefClass(
                        "{nms}.EnumPlayerInfoAction")
                                                                                                .getField("ADD_PLAYER"),
                                                                                 npc);

        Object packetPlayOutNamedEntitySpawn = ReflectionUtils.getRefClass("{nms}.PacketPlayOutNamedEntitySpawn")
                                                              .getConstructor().create(npc);

        player.sendPacket(packetPlayOutPlayerInfo);
        player.sendPacket(packetPlayOutNamedEntitySpawn);
    }

}
