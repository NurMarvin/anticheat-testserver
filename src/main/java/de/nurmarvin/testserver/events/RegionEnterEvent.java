package de.nurmarvin.testserver.events;

import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.region.Region;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Data
@EqualsAndHashCode(callSuper = false)
public class RegionEnterEvent extends Event implements Cancellable {
    private static final HandlerList HANDLER_LIST = new HandlerList();
    private Region region;
    private Player player;
    private boolean cancelled = false;

    public RegionEnterEvent(Player who, Region region) {
        this.player = who;
        this.region = region;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }
}
