package de.nurmarvin.testserver.events;

import de.nurmarvin.testserver.entities.Player;
import de.nurmarvin.testserver.permissions.Rank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Data
@EqualsAndHashCode(callSuper = false)
public class RankUpdateEvent extends Event {
    private static final HandlerList HANDLER_LIST = new HandlerList();

    private Player player;
    private Rank oldRank;
    private Rank newRank;

    public RankUpdateEvent(Player player, Rank oldRank, Rank newRank) {
        this.player = player;
        this.oldRank = oldRank;
        this.newRank = newRank;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }
}
