package de.nurmarvin.testserver.utils;

import de.nurmarvin.testserver.Testserver;
import de.nurmarvin.testserver.entities.Player;
import lombok.Data;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;

@Data
public class Server {
    private boolean hungerEnabled;
    private boolean damageEnabled;
    private boolean maintenance;
    private boolean achievements;
    private boolean scoreboardEnabled;
    private boolean weatherEnabled;
    private boolean borderEnabled;
    private boolean redstoneEnabked;
    private String name;

    public Server() {
        this.name = "Not set";
        this.hungerEnabled = false;
        this.damageEnabled = false;
        this.maintenance = false;
        this.achievements = false;
        this.scoreboardEnabled = true;
        this.weatherEnabled = false;
        this.borderEnabled = true;
        this.redstoneEnabked = true;
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Testserver.getInstance(), new Lag(), 100L, 1L);
    }

    public Player[] getPlayers() {
        List<Player> players = new ArrayList<>();

        for (org.bukkit.entity.Player player : Bukkit.getOnlinePlayers())
            players.add(Testserver.getInstance().getPlayerManager().getPlayer(player.getUniqueId()));

        return players.toArray(new Player[players.size()]);
    }

    public double getTPS() {
        return Math.min(20.0, UtilMath.trim(0, Lag.getTPS()));
    }
}
