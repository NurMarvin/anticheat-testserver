package de.nurmarvin.testserver.utils;

import net.md_5.bungee.api.ChatColor;

public class UtilString {
    public static String getBars(double current, double max, int bars, boolean reverse) {
        double percent = current / max;
        ChatColor color = reverse ? ChatColor.DARK_RED : ChatColor.GREEN;

        if (percent <= 0.10) {
            color = reverse ? ChatColor.GREEN : ChatColor.DARK_RED;
        } else if (percent <= 0.25) {
            color = reverse ? ChatColor.YELLOW : ChatColor.RED;
        } else if (percent <= 0.50) {
            color = ChatColor.GOLD;
        } else if (percent <= 0.75) {
            color = reverse ? ChatColor.RED : ChatColor.YELLOW;
        }

        long coloredBars = Math.round(bars * percent);

        String healthBar = color + "";
        for (int i = 0; i < coloredBars; i++) {
            healthBar += "█";
        }

        healthBar += ChatColor.GRAY + "";

        for (int i = 0; i < (bars - coloredBars); i++) {
            healthBar += "█";
        }

        return healthBar;
    }
}
