package de.nurmarvin.testserver.utils;

import de.nurmarvin.testserver.Testserver;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class SnowEffectRunnable implements Runnable {

    @Override
    public void run() {
        List<Block> blocks = Testserver.getInstance().getRegionManager().getRegion("map").getCuboid()
                                       .getBlocksAtY(255);

        for (Block block : blocks)
            if (ThreadLocalRandom.current().nextInt(500) < 1)
                Bukkit.getScheduler().runTaskLater(Testserver.getInstance(),
                                                   () -> block.getLocation().getWorld()
                                                              .spawnEntity(block.getLocation(), EntityType.SNOWBALL),
                                                   ThreadLocalRandom.current().nextInt(10));
    }
}
