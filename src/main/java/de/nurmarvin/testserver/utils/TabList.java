package de.nurmarvin.testserver.utils;

import de.nurmarvin.testserver.entities.Player;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

@Data
@AllArgsConstructor
public class TabList {

    private String header;
    private String footer;

    public void send(Player player) {
        send(player, this.header, this.footer);
    }


    private void send(Player player, String header, String footer) {
        try {
            if (ReflectionUtils.getServerVersion().equalsIgnoreCase("v1_8_R2")
                || ReflectionUtils.getServerVersion().equalsIgnoreCase("v1_8_R3")) {
                final Object headerComponent = ReflectionUtils.getRefClass("{nms}.IChatBaseComponent$ChatSerializer")
                                                              .getMethod("a", String.class).getRealMethod()
                                                              .invoke(null, "{'text': '" + header + "'}");

                final Object footerComponent = ReflectionUtils.getRefClass("{nms}.IChatBaseComponent$ChatSerializer")
                                                              .getMethod("a", String.class).getRealMethod()
                                                              .invoke(null, "{'text': '" + footer + "'}");
                final Object packet = ReflectionUtils.getRefClass("{nms}.PacketPlayOutPlayerListHeaderFooter")
                                                     .getConstructor(
                                                             ReflectionUtils.getRefClass("{nms}.IChatBaseComponent"))
                                                     .getRealConstructor()
                                                     .newInstance(headerComponent);
                final Field f = packet.getClass().getDeclaredField("b");
                f.setAccessible(true);
                f.set(packet, footerComponent);
                final Object nmsp2 = player.getBukkitPlayer().getClass()
                                           .getMethod("getHandle", (Class<?>[]) new Class[0])
                                           .invoke(player.getBukkitPlayer(), new Object[0]);
                final Object pcon2 = nmsp2.getClass().getField("playerConnection").get(nmsp2);
                pcon2.getClass().getMethod("sendPacket", ReflectionUtils.getRefClass("{nms}.Packet").getRealClass())
                     .invoke(pcon2, packet);
            } else {
                final Object headerComponent = ReflectionUtils.getRefClass("{nms}.ChatSerializer")
                                                              .getMethod("a", String.class).getRealMethod()
                                                              .invoke(null, "{'text': '" + header + "'}");
                final Object footerComponent = ReflectionUtils.getRefClass("{nms}.ChatSerializer")
                                                              .getMethod("a", String.class).getRealMethod()
                                                              .invoke(null, "{'text': '" + footer + "'}");
                final Object packet = ReflectionUtils.getRefClass("{nms}.PacketPlayOutPlayerListHeaderFooter")
                                                     .getConstructor(
                                                             ReflectionUtils.getRefClass("{nms}.IChatBaseComponent"))
                                                     .getRealConstructor()
                                                     .newInstance(headerComponent);
                final Field f = packet.getClass().getDeclaredField("b");
                f.setAccessible(true);
                f.set(packet, footerComponent);
                final Object nmsp2 = player.getBukkitPlayer().getClass()
                                           .getMethod("getHandle", (Class<?>[]) new Class[0])
                                           .invoke(player.getBukkitPlayer(), new Object[0]);
                final Object pcon2 = nmsp2.getClass().getField("playerConnection").get(nmsp2);
                pcon2.getClass().getMethod("sendPacket", ReflectionUtils.getRefClass("{nms}.Packet").getRealClass())
                     .invoke(pcon2, packet);
            }
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchMethodException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
