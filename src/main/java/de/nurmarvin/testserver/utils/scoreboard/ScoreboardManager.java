package de.nurmarvin.testserver.utils.scoreboard;

import de.nurmarvin.testserver.entities.Player;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class ScoreboardManager {
    public static void loadTeam(Scoreboard scoreboard, Player player) {

        String idString = "";

        while (idString.length() + String.valueOf(player.getRank().ordinal()).length() < 3)
            idString += "0";

        String teamName = idString + player.getRank().ordinal() + player.getRank().name();

        Team team = scoreboard.getTeam(teamName);
        if (team == null) {
            team = scoreboard.registerNewTeam(teamName);
            team.setPrefix(player.getRank().getPrefix());
        }

        if (!team.hasPlayer(player.getBukkitPlayer())) {
            team.addPlayer(player.getBukkitPlayer());
        }
    }

    public static void updateScoreboard(Player player) {
        for (Player players : player.getServer().getPlayers()) {

            Scoreboard scoreboard = players.getBukkitPlayer().getScoreboard();

            if (scoreboard == null)
                scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

            for (Player all : player.getServer().getPlayers()) {
                ScoreboardManager.loadTeam(scoreboard, all);
            }

            players.getBukkitPlayer().setScoreboard(scoreboard);
        }
    }
}
