package de.nurmarvin.testserver.utils;

import com.google.common.collect.Sets;
import de.nurmarvin.testserver.Testserver;
import fr.neatmonster.nocheatplus.actions.ParameterName;
import fr.neatmonster.nocheatplus.checks.CheckType;
import fr.neatmonster.nocheatplus.checks.ViolationData;
import fr.neatmonster.nocheatplus.checks.access.IViolationInfo;
import fr.neatmonster.nocheatplus.hooks.APIUtils;
import fr.neatmonster.nocheatplus.hooks.NCPHook;
import fr.neatmonster.nocheatplus.utilities.TickTask;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Set;

public class NCPLogHook implements NCPHook {
    private final DecimalFormat format = new DecimalFormat("#.###");

    private final Set<ParameterName> detailsUsed = Sets.newLinkedHashSet();

    private final long[] lagTicks = new long[]{1, 5, 10, 20, 1200};

    public NCPLogHook() {
        detailsUsed.addAll(Arrays.asList(ParameterName.values()));
        // Retrace information that's not meant to be public or is given otherwise.
        for (final ParameterName param : new ParameterName[]{
                ParameterName.IP,
                ParameterName.PLAYER,
                ParameterName.PLAYER_NAME,
                ParameterName.VIOLATIONS,
                ParameterName.CHECK,
                ParameterName.LOCATION_FROM,
                ParameterName.LOCATION_TO,
                ParameterName.WORLD,
                ParameterName.UUID
        }) {
            detailsUsed.remove(param);
        }
    }

    @Override
    public String getHookName() {
        return "NCPLogHook";
    }

    @Override
    public String getHookVersion() {
        return "1.0";
    }

    @Override
    public boolean onCheckFailure(final CheckType checkType, final Player player, final IViolationInfo info) {
        if (APIUtils.needsSynchronization(checkType))
            Testserver.getInstance().runAsync(() -> log(checkType, player, info));
        else
            log(checkType, player, info);
        return false;
    }

    public void log(final CheckType checkType, final Player player, final IViolationInfo info) {

        de.nurmarvin.testserver.entities.Player realPlayer =
                Testserver.getInstance().getPlayerManager().getPlayer(player.getUniqueId());

        // Build the message.
        final StringBuilder builder = new StringBuilder(250);
        // Name
        builder.append(ChatColor.YELLOW + "[LOG] " + ChatColor.WHITE).append(realPlayer.getNameWithPrefix())
               .append(" ");
        // Check
        builder.append(ChatColor.GRAY).append("failed ").append(ChatColor.GREEN).append(checkType.name());
        // VL
        builder.append(ChatColor.YELLOW + " vl ").append(ChatColor.GOLD).append(format.format(info.getTotalVl()))
               .append(" (+")
               .append(format.format(info.getAddedVl())).append(")");
        // Parameters
        builder.append(ChatColor.GRAY).append(" cancelled=")
               .append(info.willCancel() ? ChatColor.GREEN + "true" : ChatColor.RED + "false");

        builder.append(ChatColor.GRAY + " Details:");
        final ViolationData data = (ViolationData) info;
        for (final ParameterName param : detailsUsed) {
            final String val = data.getParameter(param);
            final String text = param.getText();
            if (val != null && !val.equals("[" + text + "]") &&
                (param != ParameterName.PLAYER_DISPLAY_NAME || !player.getName().equals(val))) {
                builder.append(" ").append(text).append("=").append(val);
            }
        }
        // Lag
        final String[] lagSpecs = new String[lagTicks.length];
        boolean hasLag = false;
        for (int i = 0; i < lagTicks.length; i++) {
            final long ticks = lagTicks[i];
            final float lag = TickTask.getLag(50L * ticks);
            if (lag > 1.0) {
                hasLag = true;
                lagSpecs[i] = " " + lagTicks[i] + "@" + format.format(lag);
            } else {
                lagSpecs[i] = null;
            }
        }
        if (hasLag) {
            builder.append(" Ticks[Lag]:");
            for (int i = 0; i < lagTicks.length; i++) {
                if (lagSpecs[i] != null) {
                    builder.append(lagSpecs[i]);
                }
            }
        }
        final String msg = builder.toString();

        player.sendMessage(msg);
    }
}
