package de.nurmarvin.testserver.utils;

import de.nurmarvin.testserver.Testserver;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.CraftSound;
import org.bukkit.craftbukkit.v1_8_R3.block.CraftBlock;
import org.bukkit.scheduler.BukkitTask;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

public class BuildAreaRunnable implements Runnable {
    private int counter;
    private BukkitTask bukkitTask;

    @Override
    public void run() {
        counter = 0;

        List<Block> blocks = Testserver.getInstance().getRegionManager().getRegion("buildarea").getCuboid()
                                       .getBlocks();

        bukkitTask = Bukkit.getScheduler().runTaskTimerAsynchronously(Testserver.getInstance(), () ->
        {
            if (blocks.size() <= counter) {
                Testserver.getInstance().getRegionManager().getRegion("buildareatop").getCuboid()
                          .getBlocks().forEach(block -> {
                    if (block.getType() != Material.AIR) {
                        Testserver.getInstance().runSync(() -> breakBlock(block));
                    }
                });

                Testserver.getInstance().getServer().broadcastMessage("§eThe build area was reset!");
                this.bukkitTask.cancel();
                return;
            }

            for (int i = 0; i < 260; i++) {
                Block block = blocks.get(counter);
                counter++;

                resetBlock(block);

                if (blocks.size() <= counter) {
                    return;
                }
            }

        }, 0, 1);
    }

    public void breakBlock(Block b) {
        try {
            b.setType(Material.AIR);
            for (Sound sound : Sound.values()) {
                Field f = CraftSound.class.getDeclaredField("sounds");
                f.setAccessible(true);

                String[] sounds = (String[]) f.get(null);
                Method getBlock = CraftBlock.class.getDeclaredMethod("getNMSBlock");
                getBlock.setAccessible(true);
                Object nmsBlock = getBlock.invoke(b);
                net.minecraft.server.v1_8_R3.Block block = (net.minecraft.server.v1_8_R3.Block) nmsBlock;

                if (block.stepSound.getBreakSound()
                                   .equals(sounds[sound.ordinal()])) {
                    b.getWorld().playSound(b.getLocation(), sound, 10, 10);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetBlock(Block block) {
        if (block.getY() < 63) {
            if (block.getType() != Material.DIRT)
                Testserver.getInstance()
                          .runSync(() -> block.getWorld().playSound(block.getLocation(), Sound.ITEM_PICKUP, 1, 1));
            Testserver.getInstance().runSync(() -> block.setType(Material.DIRT));
        }
        if (block.getY() == 63) {
            if (block.getType() != Material.GRASS)
                Testserver.getInstance()
                          .runSync(() -> block.getWorld().playSound(block.getLocation(), Sound.ITEM_PICKUP, 1, 1));
            Testserver.getInstance().runSync(() -> block.setType(Material.GRASS));
        }
    }
}
