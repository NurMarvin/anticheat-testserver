package de.nurmarvin.testserver.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.bukkit.plugin.Plugin;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileManager {

    private static final Gson gson = new Gson();
    private File file;

    public FileManager(Plugin plugin) {
        File folder = plugin.getDataFolder();
        if (!folder.exists()) {
            if (!folder.mkdir()) {
                System.out.println("Error while creating folder for the plugin!");
                return;
            }
        }
        this.file = new File(plugin.getDataFolder(), "mysql.json");

        if (!this.file.exists()) {
            try {
                if (this.file.createNewFile()) {
                    System.out.println("Error while creating file for the plugin!");
                }
                JsonObject jsonObject = new JsonObject();

                jsonObject.addProperty("username", "username");
                jsonObject.addProperty("password", "password");
                jsonObject.addProperty("port", 3306);
                jsonObject.addProperty("database", "hypixel");
                jsonObject.addProperty("hostname", "localhost");

                Writer writer =
                        new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8));
                writer.write(gson.toJson(jsonObject));
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public File getFile() {
        return file;
    }

    public String[] read() {
        try {
            JsonElement element =
                    gson.fromJson(new InputStreamReader(new FileInputStream(this.file)), JsonObject.class);

            if (element.isJsonObject()) {
                JsonObject jsonObject = element.getAsJsonObject();
                return new String[]{
                        "jdbc:mysql://" + jsonObject.get("hostname").getAsString() + "/" +
                        jsonObject.get("database").getAsString(),
                        jsonObject.get("username").getAsString(),
                        jsonObject.get("password").getAsString()
                };
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return new String[0];
    }
}
