package de.nurmarvin.testserver.utils;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import de.nurmarvin.testserver.Testserver;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.logging.Level;

@Data
public class LocationManager {
    private Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    private HashMap<String, Location> locations;
    private File locationsFile;

    public LocationManager() {
        this.locations = Maps.newHashMap();
        this.locationsFile = new File(Testserver
                                              .getInstance().getDataFolder(), "locations.json");

        if (!this.locationsFile.exists()) {
            try {
                this.locationsFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("-------- Starting to load hologramLocations --------");
        try {
            JsonArray array = gson.fromJson(new InputStreamReader(new FileInputStream(locationsFile),
                                                                  StandardCharsets.UTF_8), JsonArray.class);

            if (array == null)
                return;

            array.forEach(element -> {
                JsonObject obj;
                if (element.isJsonObject()) {
                    obj = element.getAsJsonObject();
                    if (obj.has("name")) {
                        String name = obj.get("name").getAsString();
                        Location location = new Location(Testserver.getInstance().getServer().getWorld(obj.get("world")
                                                                                                          .getAsString()),
                                                         obj.get("x").getAsDouble(),
                                                         obj.get("y").getAsDouble(),
                                                         obj.get("z").getAsDouble());

                        location.setYaw(obj.get("yaw").getAsFloat());
                        location.setPitch(obj.get("pitch").getAsFloat());

                        this.locations.put(name, location);
                    }
                }
            });
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public Location getLocation(String name) {
        return this.locations.get(name.toLowerCase());
    }

    public void setLocation(String name, Location location) {
        this.locations.put(name.toLowerCase(), location);
    }

    public void removeLocation(String name) {
        if (!this.locations.containsKey(name.toLowerCase()))
            this.locations.remove(name.toLowerCase());
    }

    public void saveLocations() {
        JsonArray array = new JsonArray();

        this.locations.forEach((s, l) -> {
            JsonObject location = new JsonObject();

            location.addProperty("name", s);
            location.addProperty("world", l.getWorld().getName());
            location.addProperty("x", l.getX());
            location.addProperty("y", l.getY());
            location.addProperty("z", l.getZ());
            location.addProperty("yaw", l.getYaw());
            location.addProperty("pitch", l.getPitch());

            array.add(location);
        });

        Writer writer;

        try {
            writer = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream(locationsFile), StandardCharsets.UTF_8));
            writer.write(gson.toJson(array));
            writer.close();
        } catch (IOException e) {
            Bukkit.getLogger().log(Level.SEVERE, "Failed to save hologramLocations file", e);
        }
    }
}
