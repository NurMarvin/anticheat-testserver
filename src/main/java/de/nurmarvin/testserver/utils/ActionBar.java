package de.nurmarvin.testserver.utils;

import de.nurmarvin.testserver.entities.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.md_5.bungee.api.ChatMessageType;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;

@Data
@AllArgsConstructor
public class ActionBar {
    private String message;

    public void send(Player player) {
        if (message == null) message = "";
        message = ChatColor.translateAlternateColorCodes('&', message);

        IChatBaseComponent chat = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\"}");
        PacketPlayOutChat packet = new PacketPlayOutChat(chat, (byte) ChatMessageType.ACTION_BAR.ordinal());
        ((CraftPlayer) player.getBukkitPlayer()).getHandle().playerConnection.sendPacket(packet);
    }
}
