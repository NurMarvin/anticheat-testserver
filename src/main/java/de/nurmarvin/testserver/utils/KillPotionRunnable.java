package de.nurmarvin.testserver.utils;

import de.nurmarvin.testserver.Testserver;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;
import org.bukkit.scheduler.BukkitTask;

import java.util.List;

public class KillPotionRunnable implements Runnable {
    private int counter;
    private BukkitTask bukkitTask;

    @Override
    public void run() {
        counter = 0;

        List<Block> blocks = Testserver.getInstance().getRegionManager().getRegion("map").getCuboid()
                                                                   .getBlocksAtY(75);

        bukkitTask = Bukkit.getScheduler().runTaskTimerAsynchronously(Testserver.getInstance(), () ->
        {
            if (blocks.size() <= counter) {
                this.bukkitTask.cancel();
                return;
            }

            for (int i = 0; i < 10; i++) {
                Block block = blocks.get(counter);
                counter++;

                for (int j = 0; j < 10; j++)
                    Testserver.getInstance().runSync(() -> spawnPotion(block));

                if (blocks.size() <= counter) {
                    return;
                }
            }

        }, 0, 1);
    }

    public void spawnPotion(Block block) {
        Potion potion = new Potion(PotionType.INSTANT_DAMAGE, 2);
        potion.setSplash(true);

        ItemStack itemStack = new ItemStack(Material.POTION);
        potion.apply(itemStack);

        ThrownPotion thrownPotion = block.getWorld().spawn(block.getLocation(), ThrownPotion.class);
        thrownPotion.setItem(itemStack);
    }
}
