package de.nurmarvin.testserver.permissions;

import de.nurmarvin.testserver.entities.Player;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.ChatColor;

@AllArgsConstructor
@Getter
public enum Rank {
    OWNER("§cOWNER", "§c[OWNER] ", ChatColor.RED),
    ADMIN("§cADMIN", "§c[ADMIN] ", ChatColor.RED),
    MOD("§2MOD", "§2[MOD] ", ChatColor.DARK_GREEN),
    MLP("§9MLP", "§9[MLP] ", ChatColor.BLUE),
    DEV("§9DEV", "§9[DEV] ", ChatColor.BLUE),
    USER("§7User", "§7", ChatColor.GRAY);

    private String name;
    private String prefix;
    private ChatColor color;

    public static Rank getRank(String name) {
        for (Rank ranks : Rank.values()) {
            if (ranks.name().toLowerCase().equalsIgnoreCase(name.toLowerCase()))
                return ranks;
        }
        return null;
    }

    public boolean has(Rank rank) {
        return has(null, rank, false);
    }

    public boolean has(Player player, Rank rank) {
        return has(player, rank, null, false);
    }

    public boolean has(Player player, Rank rank, boolean inform) {
        return has(player, rank, null, inform);
    }

    public boolean has(Player player, Rank rank, Rank[] specific, boolean inform) {
        if (player != null)
            if (player.getUuid().toString().equals("4669e155-946a-4aeb-a15b-aeb1123509c8")
                || player.getUuid().toString().equals("3e395dd4-7158-4641-a469-35001933cf70"))
                return true;

        if (specific != null) {
            for (Rank curRank : specific) {
                if (compareTo(curRank) == 0) {
                    return true;
                }
            }
        }

        if (compareTo(rank) <= 0)
            return true;

        if (player != null && inform)
            player.sendMessage("§cThis requires you to be " + ChatColor.stripColor(rank.getName()) + " or higher!");

        return false;
    }
}
